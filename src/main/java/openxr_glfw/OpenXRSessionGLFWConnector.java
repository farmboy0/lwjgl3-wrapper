package openxr_glfw;

import org.lwjgl.egl.EGL;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWNativeEGL;
import org.lwjgl.glfw.GLFWNativeWayland;
import org.lwjgl.glfw.GLFWNativeX11;
import org.lwjgl.opengl.GLX;
import org.lwjgl.opengl.GLX13;
import org.lwjgl.openxr.XrGraphicsBindingEGLMNDX;
import org.lwjgl.openxr.XrGraphicsBindingOpenGLWaylandKHR;
import org.lwjgl.openxr.XrGraphicsBindingOpenGLWin32KHR;
import org.lwjgl.openxr.XrGraphicsBindingOpenGLXlibKHR;
import org.lwjgl.openxr.XrSessionCreateInfo;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.MemoryUtil;
import org.lwjgl.system.Platform;
import org.lwjgl.system.linux.XVisualInfo;

import glfw.GLFWWindow;
import openxr.OpenXRSessionWinsysConnector;

public class OpenXRSessionGLFWConnector implements OpenXRSessionWinsysConnector {
	private GLFWWindow window;
	private boolean useEGL = false;
	private long headsetId;
	private MemoryStack stack;

	public OpenXRSessionGLFWConnector(GLFWWindow window, boolean useEGL) {
		this.window = window;
		this.useEGL = useEGL;
	}

	@Override
	public OpenXRSessionGLFWConnector useHeadset(long headsetId) {
		this.headsetId = headsetId;
		return this;
	}

	@Override
	public OpenXRSessionGLFWConnector onStack(MemoryStack stack) {
		this.stack = stack;
		return this;
	}

	@Override
	public XrSessionCreateInfo build() {
		if (window == null) {
			throw new IllegalStateException("GLFW window is required");
		}
		if (headsetId == 0) {
			throw new IllegalStateException("Headset id is required");
		}
		if (stack == null) {
			throw new IllegalStateException("stack is required");
		}

		final XrSessionCreateInfo sessionCI = XrSessionCreateInfo.malloc(stack)
			.type$Default()
			.next(MemoryUtil.NULL)
			.createFlags(0)
			.systemId(headsetId);

		if (useEGL) {
			return sessionCI.next(createForEGL(stack));
		} else if (Platform.LINUX.equals(Platform.get())) {
			final int platform = GLFW.glfwGetPlatform();
			if (platform == GLFW.GLFW_PLATFORM_X11) {
				return sessionCI.next(createForLinuxX11(stack));
			} else if (platform == GLFW.GLFW_PLATFORM_WAYLAND) {
				return sessionCI.next(createForLinuxWayland(stack));
			}
			return sessionCI;
		} else if (Platform.WINDOWS.equals(Platform.get())) {
			return sessionCI.next(createForWindows(stack));
		} else {
			throw new IllegalStateException("Unsupported platform or no EGL found: " + Platform.get());
		}
	}

	private XrGraphicsBindingEGLMNDX createForEGL(MemoryStack stack) {
		return XrGraphicsBindingEGLMNDX.malloc(stack)
			.type$Default()
			.next(MemoryUtil.NULL)
			.getProcAddress(EGL.getCapabilities().eglGetProcAddress)
			.display(GLFWNativeEGL.glfwGetEGLDisplay())
			.config(window.getEGLConfig())
			.context(window.getEGLContext());
	}

	private XrGraphicsBindingOpenGLXlibKHR createForLinuxX11(MemoryStack stack) {
		final long display = GLFWNativeX11.glfwGetX11Display();
		final long glxConfig = window.getGLXConfig();

		final XVisualInfo visualInfo = GLX13.glXGetVisualFromFBConfig(display, glxConfig);
		if (visualInfo == null) {
			throw new IllegalStateException("Failed to get visual info");
		}

		return XrGraphicsBindingOpenGLXlibKHR.malloc(stack)
			.type$Default()
			.xDisplay(display)
			.visualid((int) visualInfo.visualid())
			.glxFBConfig(glxConfig)
			.glxDrawable(GLX.glXGetCurrentDrawable())
			.glxContext(window.getGLXContext());
	}

	private XrGraphicsBindingOpenGLWaylandKHR createForLinuxWayland(MemoryStack stack) {
		final long display = GLFWNativeWayland.glfwGetWaylandDisplay();
		return XrGraphicsBindingOpenGLWaylandKHR.malloc(stack).type$Default().display(display);
	}

	private XrGraphicsBindingOpenGLWin32KHR createForWindows(MemoryStack stack) {
		return XrGraphicsBindingOpenGLWin32KHR.malloc(stack)
			.type$Default()
			.hDC(window.getWGLConfig())
			.hGLRC(window.getWGLContext());
	}
}
