package glfw;

import org.lwjgl.glfw.GLFW;

public class GLFWWindowHintsBuilder {
	public static final GLFWWindowHintsBuilder INSTANCE = new GLFWWindowHintsBuilder();

	private boolean defaults = false;
	private boolean visible = true;
	private boolean resizable = true;
	private boolean doubleBuffered = true;
	private int openGLMajorVersion = 0;
	private int openGLMinorVersion = 0;
	private Boolean openGLCoreProfile = null;
	private boolean openGLOnEGL = false;

	private GLFWWindowHintsBuilder() {
	}

	public GLFWWindowHintsBuilder reset() {
		this.defaults = true;
		return this;
	}

	public GLFWWindowHintsBuilder visible(boolean visible) {
		this.visible = visible;
		return this;
	}

	public GLFWWindowHintsBuilder resizable(boolean resizable) {
		this.resizable = resizable;
		return this;
	}

	public GLFWWindowHintsBuilder doubleBuffered(boolean doubleBuffered) {
		this.doubleBuffered = doubleBuffered;
		return this;
	}

	public GLFWWindowHintsBuilder openGLMajorVersion(int openGLMajorVersion) {
		this.openGLMajorVersion = openGLMajorVersion;
		return this;
	}

	public GLFWWindowHintsBuilder openGLMinorVersion(int openGLMinorVersion) {
		this.openGLMinorVersion = openGLMinorVersion;
		return this;
	}

	public GLFWWindowHintsBuilder useCoreProfile() {
		this.openGLCoreProfile = true;
		return this;
	}

	public GLFWWindowHintsBuilder useCompatProfile() {
		this.openGLCoreProfile = false;
		return this;
	}

	public GLFWWindowHintsBuilder useAnyProfile() {
		this.openGLCoreProfile = null;
		return this;
	}

	public GLFWWindowHintsBuilder useEGL(boolean useEGL) {
		this.openGLOnEGL = useEGL;
		return this;
	}

	public void build() throws GLFWException {
		GLFWRuntime.initialize();
		if (defaults)
			GLFW.glfwDefaultWindowHints();
		GLFW.glfwWindowHint(GLFW.GLFW_VISIBLE, boolean2int(visible));
		GLFW.glfwWindowHint(GLFW.GLFW_RESIZABLE, boolean2int(resizable));
		GLFW.glfwWindowHint(GLFW.GLFW_DOUBLEBUFFER, boolean2int(doubleBuffered));
		GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_DEBUG, boolean2int(true)); // TODO
		if (openGLMajorVersion > 0)
			GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_VERSION_MAJOR, openGLMajorVersion);
		if (openGLMinorVersion > 0)
			GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_VERSION_MINOR, openGLMinorVersion);
		if (Boolean.TRUE.equals(openGLCoreProfile))
			GLFW.glfwWindowHint(GLFW.GLFW_OPENGL_PROFILE, GLFW.GLFW_OPENGL_CORE_PROFILE);
		if (Boolean.FALSE.equals(openGLCoreProfile))
			GLFW.glfwWindowHint(GLFW.GLFW_OPENGL_PROFILE, GLFW.GLFW_OPENGL_COMPAT_PROFILE);
		if (openGLOnEGL) {
			GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_CREATION_API, GLFW.GLFW_EGL_CONTEXT_API);
		}
	}

	private int boolean2int(boolean b) {
		return b ? GLFW.GLFW_TRUE : GLFW.GLFW_FALSE;
	}
}
