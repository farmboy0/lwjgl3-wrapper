package glfw;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWCursorPosCallbackI;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWNativeEGL;
import org.lwjgl.glfw.GLFWNativeGLX;
import org.lwjgl.glfw.GLFWNativeWGL;
import org.lwjgl.glfw.GLFWNativeWin32;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.system.MemoryUtil;
import org.lwjgl.system.windows.User32;

public class GLFWWindow {

	private final long window;

	public GLFWWindow(int width, int height, String title) throws GLFWException {
		GLFWRuntime.initialize();
//		GLFW.glfwDefaultWindowHints();
//		GLFW.glfwWindowHint(GLFW.GLFW_VISIBLE, GLFW.GLFW_FALSE);
//		GLFW.glfwWindowHint(GLFW.GLFW_RESIZABLE, GLFW.GLFW_FALSE);

		window = GLFW.glfwCreateWindow(width, height, title, MemoryUtil.NULL, MemoryUtil.NULL);
		if (window == MemoryUtil.NULL)
			throw new GLFWException("Failed to create the window");
	}

	public void _GLFWWindow_() {
		GLFW.glfwDestroyWindow(window);
	}

	public void close() {
		GLFW.glfwSetWindowShouldClose(window, true);
	}

	public double getAspectRatio() {
		int[] width = new int[1];
		int[] height = new int[1];
		GLFW.glfwGetWindowSize(window, width, height);
		return width[0] / height[0];
	}

	public int getHeight() {
		int[] height = new int[1];
		GLFW.glfwGetWindowSize(window, null, height);
		return height[0];
	}

	public int getWidth() {
		int[] width = new int[1];
		GLFW.glfwGetWindowSize(window, width, null);
		return width[0];
	}

	public boolean isClosing() {
		return GLFW.glfwWindowShouldClose(window);
	}

	public void register(GLFWKeyHandler keyHandler) {
		GLFW.glfwSetKeyCallback(window, new GLFWKeyCallback() {
			@Override
			public void invoke(long window, int key, int scancode, int action, int mods) {
				keyHandler.handle(key, scancode, action, mods);
			}
		});
	}

	public void register(GLFWCursorPosHandler cpHandler) {
		GLFW.glfwSetCursorPosCallback(window, new GLFWCursorPosCallbackI() {
			@Override
			public void invoke(long window, double xpos, double ypos) {
				cpHandler.handle(xpos, ypos);
			}
		});
	}

	public void centerOnScreen() {
		GLFWVidMode vidmode = GLFW.glfwGetVideoMode(GLFW.glfwGetPrimaryMonitor());
		int[] width = new int[1];
		int[] height = new int[1];
		GLFW.glfwGetWindowSize(window, width, height);
		GLFW.glfwSetWindowPos(window, (vidmode.width() - width[0]) / 2, (vidmode.height() - height[0]) / 2);
	}

	public void show() {
		GLFW.glfwShowWindow(window);
	}

	public void makeCurrent() {
		GLFW.glfwMakeContextCurrent(window);
	}

	public void setSwapInterval(int swapInterval) {
		makeCurrent();
		GLFW.glfwSwapInterval(swapInterval);
	}

	public void swapBuffers() {
		GLFW.glfwSwapBuffers(window);
	}

	public long getEGLConfig() {
		return GLFWNativeEGL.glfwGetEGLConfig(window);
	}

	public long getEGLContext() {
		return GLFWNativeEGL.glfwGetEGLContext(window);
	}

	public long getGLXConfig() {
		return GLFWNativeGLX.glfwGetGLXFBConfig(window);
	}

	public long getGLXContext() {
		return GLFWNativeGLX.glfwGetGLXContext(window);
	}

	public long getWGLConfig() {
		return User32.GetDC(GLFWNativeWin32.glfwGetWin32Window(window));
	}

	public long getWGLContext() {
		return GLFWNativeWGL.glfwGetWGLContext(window);
	}
}
