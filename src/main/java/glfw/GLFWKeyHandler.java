package glfw;

public interface GLFWKeyHandler {
	void handle(int key, int scancode, int action, int mods);
}
