package glfw;

public interface GLFWCursorPosHandler {

	void handle(double xpos, double ypos);
}
