package glfw;

public interface GLFWErrorHandler {
	void handle(int error, String text);
}
