package glfw;

public class GLFWException extends Exception {

	public GLFWException() {
	}

	public GLFWException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public GLFWException(String message, Throwable cause) {
		super(message, cause);
	}

	public GLFWException(String message) {
		super(message);
	}

	public GLFWException(Throwable cause) {
		super(cause);
	}

}
