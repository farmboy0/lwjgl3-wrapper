package glfw;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVulkan;

public class GLFWRuntime {

	private static boolean initialized = false;

	private GLFWRuntime() {
	}

	static void initialize() throws GLFWException {
		if (initialized)
			return;
		if (!GLFW.glfwInit())
			throw new GLFWException("Unable to initialize GLFW");
		initialized = true;
	}

	public static void terminate() {
		if (initialized)
			GLFW.glfwTerminate();
	}

	public static boolean isVulkanSupported() throws GLFWException {
		initialize();
		return GLFWVulkan.glfwVulkanSupported();
	}

	public static double time() {
		return GLFW.glfwGetTime();
	}

	public static void pollEvents() {
		if (initialized)
			GLFW.glfwPollEvents();
	}

	public static void register(GLFWErrorHandler errorCallback) {
		GLFW.glfwSetErrorCallback(new GLFWErrorCallback() {

			@Override
			public void invoke(int error, long description) {
				errorCallback.handle(error, getDescription(description));
			}
		});
	}
}
