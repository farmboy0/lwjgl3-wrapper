package openxr;

import static openxr.OpenXRCalls.fill;
import static openxr.OpenXRCalls.toTryCustom;
import static openxr.OpenXRCalls.toTryStack;
import static org.lwjgl.openxr.XR10.xrCreateSession;
import static org.lwjgl.openxr.XR10.xrGetSystem;

import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.util.function.IntFunction;
import java.util.function.ToIntBiFunction;
import java.util.function.ToIntFunction;

import io.vavr.API;
import io.vavr.collection.Array;
import io.vavr.collection.List;
import io.vavr.collection.Seq;
import io.vavr.collection.Set;

import org.joml.Quaternionf;
import org.joml.Quaternionfc;
import org.joml.Vector3f;
import org.joml.Vector3fc;
import org.lwjgl.PointerBuffer;
import org.lwjgl.openxr.EXTDebugUtils;
import org.lwjgl.openxr.KHROpenGLEnable;
import org.lwjgl.openxr.XR10;
import org.lwjgl.openxr.XrActionSet;
import org.lwjgl.openxr.XrActionSetCreateInfo;
import org.lwjgl.openxr.XrDebugUtilsMessengerCallbackDataEXT;
import org.lwjgl.openxr.XrDebugUtilsMessengerCallbackEXTI;
import org.lwjgl.openxr.XrDebugUtilsMessengerCreateInfoEXT;
import org.lwjgl.openxr.XrDebugUtilsMessengerEXT;
import org.lwjgl.openxr.XrEventDataBaseHeader;
import org.lwjgl.openxr.XrEventDataBuffer;
import org.lwjgl.openxr.XrEventDataEventsLost;
import org.lwjgl.openxr.XrEventDataInstanceLossPending;
import org.lwjgl.openxr.XrEventDataInteractionProfileChanged;
import org.lwjgl.openxr.XrEventDataReferenceSpaceChangePending;
import org.lwjgl.openxr.XrEventDataSessionStateChanged;
import org.lwjgl.openxr.XrGraphicsRequirementsOpenGLKHR;
import org.lwjgl.openxr.XrInstance;
import org.lwjgl.openxr.XrPosef;
import org.lwjgl.openxr.XrSession;
import org.lwjgl.openxr.XrSessionCreateInfo;
import org.lwjgl.openxr.XrSystemGetInfo;
import org.lwjgl.openxr.XrSystemGraphicsProperties;
import org.lwjgl.openxr.XrSystemProperties;
import org.lwjgl.openxr.XrSystemTrackingProperties;
import org.lwjgl.openxr.XrViewConfigurationView;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.MemoryUtil;

import openxr.OpenXRDebugListener.Channel;
import openxr.OpenXRDebugListener.Severity;

public class OpenXRInstance {
	private static final String ERR_FIND_HEADSET = "Failed to get system id of headset";
	private static final String ERR_SYSTEM_PROPS = "Failed to read system poperties";
	private static final String ERR_CREATE_SESSION = "Failed to create session";
	private static final String ERR_CREATE_DEBUGUTILS = "Failed to create debug utils";
	private static final String ERR_CREATE_ACTION_SET = "Failed to create action set";

	private XrInstance instance;

	OpenXRInstance(XrInstance xrInstance) {
		this.instance = xrInstance;
	}

	public void _OpenXRInstance_() {
		if (debugMessenger != null) {
			EXTDebugUtils.xrDestroyDebugUtilsMessengerEXT(debugMessenger);
		}
		edBuffer.free();
		XR10.xrDestroyInstance(instance);
		instance = null;
	}

	private long headsetId;

	public long getHeadSetId() {
		if (headsetId == 0) {
			headsetId = findHaedset();
		}
		return headsetId;
	}

	private long findHaedset() {
		try (MemoryStack stack = MemoryStack.stackPush()) {
			final XrSystemGetInfo sSystemInfo = XrSystemGetInfo.malloc(stack)
				.type$Default()
				.next(MemoryUtil.NULL)
				.formFactor(XR10.XR_FORM_FACTOR_HEAD_MOUNTED_DISPLAY);

			final ToIntFunction<LongBuffer> fSystemId = bSystemId -> xrGetSystem(instance, sSystemInfo, bSystemId);
			return toTryStack(stack.callocLong(1), fSystemId, ERR_FIND_HEADSET).mapTry(bSystemId -> {
				return bSystemId.get(0);
			}).get();
		}
	}

	private SystemProperties properties = null;

	public int getVendorId() {
		if (properties == null) {
			readSystemProperties();
		}
		return properties.vendorId();
	}

	public long getSystemId() {
		if (properties == null) {
			readSystemProperties();
		}
		return properties.systemId();
	}

	public String getSystemName() {
		if (properties == null) {
			readSystemProperties();
		}
		return properties.systemName();
	}

	public boolean hasOrientationTracking() {
		if (properties == null) {
			readSystemProperties();
		}
		return properties.hasOrientationTracking();
	}

	public boolean hasPositionTracking() {
		if (properties == null) {
			readSystemProperties();
		}
		return properties.hasPositionTracking();
	}

	public int getMaxLayerCount() {
		if (properties == null) {
			readSystemProperties();
		}
		return properties.maxLayerCount();
	}

	public int getMaxSwapchainImageHeight() {
		if (properties == null) {
			readSystemProperties();
		}
		return properties.maxSwapchainImageHeight();
	}

	public int getMaxSwapchainImageWidth() {
		if (properties == null) {
			readSystemProperties();
		}
		return properties.maxSwapchainImageWidth();
	}

	private void readSystemProperties() {
		try (MemoryStack stack = MemoryStack.stackPush()) {
			final ToIntFunction<XrSystemProperties> fSystemProps = sSystemProps -> XR10.xrGetSystemProperties(instance,
				getHeadSetId(), sSystemProps);

			final XrSystemProperties systemProperties = XrSystemProperties.calloc(stack).type$Default();
			properties = toTryStack(systemProperties, fSystemProps, ERR_SYSTEM_PROPS).mapTry(sSystemProps -> {
				final XrSystemTrackingProperties tracking = sSystemProps.trackingProperties();
				final XrSystemGraphicsProperties graphics = sSystemProps.graphicsProperties();
				return new SystemProperties(sSystemProps.vendorId(), sSystemProps.systemId(),
					sSystemProps.systemNameString(), tracking.orientationTracking(), tracking.positionTracking(),
					graphics.maxSwapchainImageWidth(), graphics.maxSwapchainImageHeight(), graphics.maxLayerCount());
			}).get();
		}
	}

	public OpenXRGLSupport determineGLSupport() {
		try (MemoryStack stack = MemoryStack.stackPush()) {
			final XrGraphicsRequirementsOpenGLKHR glRequirements = XrGraphicsRequirementsOpenGLKHR.calloc(stack)
				.type$Default()
				.next(MemoryUtil.NULL)
				.minApiVersionSupported(0)
				.maxApiVersionSupported(0);

			KHROpenGLEnable.xrGetOpenGLGraphicsRequirementsKHR(instance, getHeadSetId(), glRequirements);

			int minMajorVersion = XR10.XR_VERSION_MAJOR(glRequirements.minApiVersionSupported());
			int maxMajorVersion = XR10.XR_VERSION_MAJOR(glRequirements.maxApiVersionSupported());
			int minMinorVersion = XR10.XR_VERSION_MINOR(glRequirements.minApiVersionSupported());
			int maxMinorVersion = XR10.XR_VERSION_MINOR(glRequirements.maxApiVersionSupported());

			return new OpenXRGLSupport(minMajorVersion, maxMajorVersion, minMinorVersion, maxMinorVersion);
		}
	}

	public OpenXRSession createSessionFor(OpenXRSessionWinsysConnector connector) {
		try (MemoryStack stack = MemoryStack.stackPush()) {
			final XrSessionCreateInfo sessionCI = connector.useHeadset(getHeadSetId()).onStack(stack).build();

			final ToIntFunction<PointerBuffer> fSession = bSession -> xrCreateSession(instance, sessionCI, bSession);
			return toTryStack(stack.mallocPointer(1), fSession, ERR_CREATE_SESSION).mapTry(bSession -> {
				return new OpenXRSession(new XrSession(bSession.get(0), instance));
			}).get();
		}
	}

	private XrDebugUtilsMessengerEXT debugMessenger;

	public void enableDebugging(OpenXRDebugListener listener) {
		enableDebugging(listener, true, true, true, true, true, true, true, true);
	}

	public void enableDebugging(OpenXRDebugListener listener, boolean includeErrors, boolean includeWarnings,
		boolean includeInfos, boolean includeVerbose, boolean includeGeneral, boolean includeValidation,
		boolean includePerf, boolean includeConformance) {

		try (MemoryStack stack = MemoryStack.stackPush()) {
			final XrDebugUtilsMessengerCallbackEXTI cb = (severity, channels, callbackData, userData) -> {
				final XrDebugUtilsMessengerCallbackDataEXT data = XrDebugUtilsMessengerCallbackDataEXT
					.create(callbackData);
				final Seq<String> labels = Array.range(0, data.sessionLabelCount())
					.map(i -> data.sessionLabels().get(i).labelNameString());
				return listener.invoke(Severity.from(severity), Channel.from(channels), data.messageIdString(),
					data.functionNameString(), data.messageString(), labels);
			};
			final XrDebugUtilsMessengerCreateInfoEXT debugUtilsCI = XrDebugUtilsMessengerCreateInfoEXT.calloc(stack)
				.type$Default()
				.messageSeverities(msgSeverities(includeErrors, includeWarnings, includeInfos, includeVerbose))
				.messageTypes(msgTypes(includeGeneral, includeValidation, includePerf, includeConformance))
				.userCallback(cb);

			final ToIntFunction<PointerBuffer> fDebugUtils = bDebugUtils -> EXTDebugUtils
				.xrCreateDebugUtilsMessengerEXT(instance, debugUtilsCI, bDebugUtils);
			debugMessenger = toTryStack(stack.callocPointer(1), fDebugUtils, ERR_CREATE_DEBUGUTILS)
				.mapTry(bDebugUtils -> {
					return new XrDebugUtilsMessengerEXT(bDebugUtils.get(0), instance);
				})
				.get();
		}
	}

	private static long msgSeverities(boolean includeErrors, boolean includeWarnings, boolean includeInfos,
		boolean includeVerbose) {

		long result = 0;
		if (includeVerbose) {
			result |= EXTDebugUtils.XR_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT;
		}
		if (includeInfos) {
			result |= EXTDebugUtils.XR_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT;
		}
		if (includeWarnings) {
			result |= EXTDebugUtils.XR_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT;
		}
		if (includeErrors) {
			result |= EXTDebugUtils.XR_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
		}
		return result;
	}

	private long msgTypes(boolean includeGeneral, boolean includeValidation, boolean includePerf,
		boolean includeConformance) {

		long result = 0;
		if (includeGeneral) {
			result |= EXTDebugUtils.XR_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT;
		}
		if (includeValidation) {
			result |= EXTDebugUtils.XR_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT;
		}
		if (includePerf) {
			result |= EXTDebugUtils.XR_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
		}
		if (includeConformance) {
			result |= EXTDebugUtils.XR_DEBUG_UTILS_MESSAGE_TYPE_CONFORMANCE_BIT_EXT;
		}
		return result;
	}

	public Seq<ViewConfig> enumViewConfigurationViews() {
		final ToIntBiFunction<IntBuffer, XrViewConfigurationView.Buffer> fViewConfig = //
			(bCount, bViewConfig) -> XR10.xrEnumerateViewConfigurationViews(instance, getHeadSetId(),
				XR10.XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO, bCount, bViewConfig);
		final IntFunction<XrViewConfigurationView.Buffer> bufferCreator = count -> {
			return fill(XrViewConfigurationView.calloc(count), XrViewConfigurationView.TYPE,
				XR10.XR_TYPE_VIEW_CONFIGURATION_VIEW);
		};

		return toTryCustom(bufferCreator, fViewConfig, ERR_SYSTEM_PROPS).mapTry(tViewConfig -> {
			final Seq<ViewConfig> result = List.range(0, tViewConfig._1).map(tViewConfig._2::get).map(xrViewConfig -> {
				return new ViewConfig(xrViewConfig.recommendedImageRectWidth(),
					xrViewConfig.recommendedImageRectHeight(), xrViewConfig.recommendedSwapchainSampleCount(),
					xrViewConfig.maxImageRectWidth(), xrViewConfig.maxImageRectHeight(),
					xrViewConfig.maxSwapchainSampleCount());
			});
			MemoryUtil.memFree(tViewConfig._2);
			return result;
		}).get();
	}

	private Set<OpenXREventListener> listeners = API.Set();

	public void register(OpenXREventListener listener) {
		listeners = listeners.add(listener);
	}

	public void unregister(OpenXREventListener listener) {
		listeners = listeners.remove(listener);
	}

	private final XrEventDataBuffer edBuffer = XrEventDataBuffer.calloc();

	public void pollEvents() {
		boolean eventAvailable;
		do {
			eventAvailable = nextEvent();
		} while (eventAvailable);
	}

	private boolean nextEvent() {
		edBuffer.clear();
		int result = XR10.xrPollEvent(instance, edBuffer.type$Default());
		if (!XR10.XR_SUCCEEDED(result)) {
			throw new OpenXRError(result, "TODO");
		}
		if (result == XR10.XR_EVENT_UNAVAILABLE) {
			return false;
		}
		final XrEventDataBaseHeader baseHeader = XrEventDataBaseHeader.create(edBuffer.address());
		switch (baseHeader.type()) {
			case XR10.XR_TYPE_EVENT_DATA_EVENTS_LOST:
				final XrEventDataEventsLost eventsLost = XrEventDataEventsLost.create(baseHeader);
				listeners.forEach(l -> l.eventsLost(eventsLost.lostEventCount()));
				break;
			case XR10.XR_TYPE_EVENT_DATA_INSTANCE_LOSS_PENDING:
				final XrEventDataInstanceLossPending instanceLoss = XrEventDataInstanceLossPending.create(baseHeader);
				listeners.forEach(l -> l.instanceLoss(instanceLoss.lossTime()));
				break;
			case XR10.XR_TYPE_EVENT_DATA_SESSION_STATE_CHANGED:
				final XrEventDataSessionStateChanged stateChange = XrEventDataSessionStateChanged.create(baseHeader);
				listeners.forEach(l -> l.sessionStateChange(stateChange.session(),
					OpenXRSessionState.from(stateChange.state()), stateChange.time()));
				break;
			case XR10.XR_TYPE_EVENT_DATA_INTERACTION_PROFILE_CHANGED:
				final XrEventDataInteractionProfileChanged profileChange = XrEventDataInteractionProfileChanged
					.create(baseHeader);
				listeners.forEach(l -> l.interactionProfileChange(profileChange.session()));
				break;
			case XR10.XR_TYPE_EVENT_DATA_REFERENCE_SPACE_CHANGE_PENDING:
				final XrEventDataReferenceSpaceChangePending refSpaceChange = XrEventDataReferenceSpaceChangePending
					.create(baseHeader);
				final XrPosef pose = refSpaceChange.poseInPreviousSpace();
				final Vector3fc position = new Vector3f(pose.position$().x(), pose.position$().y(),
					pose.position$().z());
				final Quaternionfc orientation = new Quaternionf(pose.orientation().x(), pose.orientation().y(),
					pose.orientation().z(), pose.orientation().w());
				listeners.forEach(l -> l.refSpaceChange(refSpaceChange.session(), refSpaceChange.referenceSpaceType(),
					refSpaceChange.changeTime(), refSpaceChange.poseValid(), position, orientation));
				break;
			default:
				break;
		}
		return true;
	}

	public static record OpenXRGLSupport(int majorMin, int majorMax, int minorMin, int minorMax) {

	}

	public static record ViewConfig(int recommendedImageRectWidth, int recommendedImageRectHeight,
		int recommendedSwapchainSampleCount, int maxImageRectWidth, int maxImageRectHeight,
		int maxSwapchainSampleCount) {
	}

	private static record SystemProperties(int vendorId, long systemId, String systemName,
		boolean hasOrientationTracking, boolean hasPositionTracking, int maxSwapchainImageWidth,
		int maxSwapchainImageHeight, int maxLayerCount) {
	}

	public OpenXRActionSet createActionSet(String id, String description) {
		try (MemoryStack stack = MemoryStack.stackPush()) {
			final XrActionSetCreateInfo actionSetCI = XrActionSetCreateInfo.calloc(stack)
				.type$Default()
				.next(MemoryUtil.NULL)
				.actionSetName(stack.UTF8(id))
				.localizedActionSetName(stack.UTF8(description));

			final ToIntFunction<PointerBuffer> fActionSet = bActionSet -> XR10.xrCreateActionSet(instance, actionSetCI,
				bActionSet);
			return toTryStack(stack.mallocPointer(1), fActionSet, ERR_CREATE_ACTION_SET).mapTry(bActionSet -> {
				return new OpenXRActionSet(new XrActionSet(bActionSet.get(0), instance));
			}).get();
		}
	}
}
