package openxr;

public interface PoseBindingsBuilder extends OpenXRBindingsBuilder {
	OpenXRBindingsBuilder useLeftHandAim();

	OpenXRBindingsBuilder useRightHandAim();

	OpenXRBindingsBuilder useLeftHandGrip();

	OpenXRBindingsBuilder useRightHandGrip();
}
