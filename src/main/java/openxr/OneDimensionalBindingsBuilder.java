package openxr;

public interface OneDimensionalBindingsBuilder extends OpenXRBindingsBuilder {
	// KHR Simple
	OneDimensionalBindingsBuilder onSimpleUseLeftSelectClick();

	OneDimensionalBindingsBuilder onSimpleUseRightSelectClick();

	OneDimensionalBindingsBuilder onSimpleUseLeftMenuClick();

	OneDimensionalBindingsBuilder onSimpleUseRightMenuClick();

	// Google Daydream
	OneDimensionalBindingsBuilder onDaydreamUseLeftSelectClick();

	OneDimensionalBindingsBuilder onDaydreamUseRightSelectClick();

	OneDimensionalBindingsBuilder onDaydreamUseLeftTrackpadXValue();

	OneDimensionalBindingsBuilder onDaydreamUseRightTrackpadXValue();

	OneDimensionalBindingsBuilder onDaydreamUseLeftTrackpadYValue();

	OneDimensionalBindingsBuilder onDaydreamUseRightTrackpadYValue();

	OneDimensionalBindingsBuilder onDaydreamUseLeftTrackpadClick();

	OneDimensionalBindingsBuilder onDaydreamUseRightTrackpadClick();

	OneDimensionalBindingsBuilder onDaydreamUseLeftTrackpadTouch();

	OneDimensionalBindingsBuilder onDaydreamUseRightTrackpadTouch();

	// HTC Vive
	OneDimensionalBindingsBuilder onViveUseLeftSystemClick();

	OneDimensionalBindingsBuilder onViveUseRightSystemClick();

	OneDimensionalBindingsBuilder onViveUseLeftSqueezeClick();

	OneDimensionalBindingsBuilder onViveUseRightSqueezeClick();

	OneDimensionalBindingsBuilder onViveUseLeftMenuClick();

	OneDimensionalBindingsBuilder onViveUseRightMenuClick();

	OneDimensionalBindingsBuilder onViveUseLeftTriggerValue();

	OneDimensionalBindingsBuilder onViveUseRightTriggerValue();

	OneDimensionalBindingsBuilder onViveUseLeftTriggerClick();

	OneDimensionalBindingsBuilder onViveUseRightTriggerClick();

	OneDimensionalBindingsBuilder onViveUseLeftTrackpadXValue();

	OneDimensionalBindingsBuilder onViveUseRightTrackpadXValue();

	OneDimensionalBindingsBuilder onViveUseLeftTrackpadYValue();

	OneDimensionalBindingsBuilder onViveUseRightTrackpadYValue();

	OneDimensionalBindingsBuilder onViveUseLeftTrackpadClick();

	OneDimensionalBindingsBuilder onViveUseRightTrackpadClick();

	OneDimensionalBindingsBuilder onViveUseLeftTrackpadTouch();

	OneDimensionalBindingsBuilder onViveUseRightTrackpadTouch();

	// HTC Vive Pro
	OneDimensionalBindingsBuilder onViveProUseHeadSystemClick();

	OneDimensionalBindingsBuilder onViveProUseHeadVolumeUpClick();

	OneDimensionalBindingsBuilder onViveProUseHeadVolumeDownClick();

	OneDimensionalBindingsBuilder onViveProUseHeadMuteMicClick();

	// Microsoft WMR
	OneDimensionalBindingsBuilder onWMRUseLeftMenuClick();

	OneDimensionalBindingsBuilder onWMRUseRightMenuClick();

	OneDimensionalBindingsBuilder onWMRUseLeftSqueezeClick();

	OneDimensionalBindingsBuilder onWMRxUseRightSqueezeClick();

	OneDimensionalBindingsBuilder onWMRUseLeftTriggerValue();

	OneDimensionalBindingsBuilder onWMRUseRightTriggerValue();

	OneDimensionalBindingsBuilder onWMRUseLeftThumbstickXValue();

	OneDimensionalBindingsBuilder onWMRUseRightThumbstickXValue();

	OneDimensionalBindingsBuilder onWMRUseLeftThumbstickYValue();

	OneDimensionalBindingsBuilder onWMRUseRightThumbstickYValue();

	OneDimensionalBindingsBuilder onWMRUseLeftThumbstickClick();

	OneDimensionalBindingsBuilder onWMRUseRightThumbstickClick();

	OneDimensionalBindingsBuilder onWMRUseLeftTrackpadXValue();

	OneDimensionalBindingsBuilder onWMRUseRightTrackpadXValue();

	OneDimensionalBindingsBuilder onWMRUseLeftTrackpadYValue();

	OneDimensionalBindingsBuilder onWMRUseRightTrackpadYValue();

	OneDimensionalBindingsBuilder onWMRUseLeftTrackpadClick();

	OneDimensionalBindingsBuilder onWMRUseRightTrackpadClick();

	OneDimensionalBindingsBuilder onWMRUseLeftTrackpadTouch();

	OneDimensionalBindingsBuilder onWMRUseRightTrackpadTouch();

	// Oculus Go
	OneDimensionalBindingsBuilder onOculusGoUseLeftSystemClick();

	OneDimensionalBindingsBuilder onOculusGoUseRightSystemClick();

	OneDimensionalBindingsBuilder onOculusGoUseLeftBackClick();

	OneDimensionalBindingsBuilder onOculusGoUseRightBackClick();

	OneDimensionalBindingsBuilder onOculusGoUseLeftTriggerClick();

	OneDimensionalBindingsBuilder onOculusGoUseRightTriggerClick();

	OneDimensionalBindingsBuilder onOculusGoUseLeftTrackpadXValue();

	OneDimensionalBindingsBuilder onOculusGoUseRightTrackpadXValue();

	OneDimensionalBindingsBuilder onOculusGoUseLeftTrackpadYValue();

	OneDimensionalBindingsBuilder onOculusGoUseRightTrackpadYValue();

	OneDimensionalBindingsBuilder onOculusGoUseLeftTrackpadClick();

	OneDimensionalBindingsBuilder onOculusGoUseRightTrackpadClick();

	OneDimensionalBindingsBuilder onOculusGoUseLeftTrackpadTouch();

	OneDimensionalBindingsBuilder onOculusGoUseRightTrackpadTouch();

	// Oculus Touch
	OneDimensionalBindingsBuilder onOculusTouchUseLeftMenuClick();

	OneDimensionalBindingsBuilder onOculusTouchUseRightSystemClick();

	OneDimensionalBindingsBuilder onOculusTouchUseLeftXClick();

	OneDimensionalBindingsBuilder onOculusTouchUseRightAClick();

	OneDimensionalBindingsBuilder onOculusTouchUseLeftXTouch();

	OneDimensionalBindingsBuilder onOculusTouchUseRightATouch();

	OneDimensionalBindingsBuilder onOculusTouchUseLeftYClick();

	OneDimensionalBindingsBuilder onOculusTouchUseRightBClick();

	OneDimensionalBindingsBuilder onOculusTouchUseLeftYTouch();

	OneDimensionalBindingsBuilder onOculusTouchUseRightBTouch();

	OneDimensionalBindingsBuilder onOculusTouchUseLeftSqueezeValue();

	OneDimensionalBindingsBuilder onOculusTouchUseRightSqueezeValue();

	OneDimensionalBindingsBuilder onOculusTouchUseLeftTriggerValue();

	OneDimensionalBindingsBuilder onOculusTouchUseRightTriggerValue();

	OneDimensionalBindingsBuilder onOculusTouchUseLeftTriggerTouch();

	OneDimensionalBindingsBuilder onOculusTouchUseRightTriggerTouch();

	OneDimensionalBindingsBuilder onOculusTouchUseLeftThumbstickXValue();

	OneDimensionalBindingsBuilder onOculusTouchUseRightThumbstickXValue();

	OneDimensionalBindingsBuilder onOculusTouchUseLeftThumbstickYValue();

	OneDimensionalBindingsBuilder onOculusTouchUseRightThumbstickYValue();

	OneDimensionalBindingsBuilder onOculusTouchUseLeftThumbstickClick();

	OneDimensionalBindingsBuilder onOculusTouchUseRightThumbstickClick();

	OneDimensionalBindingsBuilder onOculusTouchUseLeftThumbstickTouch();

	OneDimensionalBindingsBuilder onOculusTouchUseRightThumbstickTouch();

	OneDimensionalBindingsBuilder onOculusTouchUseLeftThumprestTouch();

	OneDimensionalBindingsBuilder onOculusTouchUseRightThumprestTouch();

	// Valve Index
	OneDimensionalBindingsBuilder onIndexUseLeftSystemClick();

	OneDimensionalBindingsBuilder onIndexUseRightSystemClick();

	OneDimensionalBindingsBuilder onIndexUseLeftSystemTouch();

	OneDimensionalBindingsBuilder onIndexUseRightSystemTouch();

	OneDimensionalBindingsBuilder onIndexUseLeftAClick();

	OneDimensionalBindingsBuilder onIndexUseRightAClick();

	OneDimensionalBindingsBuilder onIndexUseLeftATouch();

	OneDimensionalBindingsBuilder onIndexUseRightATouch();

	OneDimensionalBindingsBuilder onIndexUseLeftBClick();

	OneDimensionalBindingsBuilder onIndexUseRightBClick();

	OneDimensionalBindingsBuilder onIndexUseLeftBTouch();

	OneDimensionalBindingsBuilder onIndexUseRightBTouch();

	OneDimensionalBindingsBuilder onIndexUseLeftSqueezeValue();

	OneDimensionalBindingsBuilder onIndexUseRightSqueezeValue();

	OneDimensionalBindingsBuilder onIndexUseLeftSqueezeForce();

	OneDimensionalBindingsBuilder onIndexUseRightSqueezeForce();

	OneDimensionalBindingsBuilder onIndexUseLeftTriggerValue();

	OneDimensionalBindingsBuilder onIndexUseRightTriggerValue();

	OneDimensionalBindingsBuilder onIndexUseLeftTriggerClick();

	OneDimensionalBindingsBuilder onIndexUseRightTriggerClick();

	OneDimensionalBindingsBuilder onIndexUseLeftTriggerTouch();

	OneDimensionalBindingsBuilder onIndexUseRightTriggerTouch();

	OneDimensionalBindingsBuilder onIndexUseLeftThumbstickXValue();

	OneDimensionalBindingsBuilder onIndexUseRightThumbstickXValue();

	OneDimensionalBindingsBuilder onIndexUseLeftThumbstickYValue();

	OneDimensionalBindingsBuilder onIndexUseRightThumbstickYValue();

	OneDimensionalBindingsBuilder onIndexUseLeftThumbstickClick();

	OneDimensionalBindingsBuilder onIndexUseRightThumbstickClick();

	OneDimensionalBindingsBuilder onIndexUseLeftThumbstickTouch();

	OneDimensionalBindingsBuilder onIndexUseRightThumbstickTouch();

	OneDimensionalBindingsBuilder onIndexUseLeftTrackpadXValue();

	OneDimensionalBindingsBuilder onIndexUseRightTrackpadXValue();

	OneDimensionalBindingsBuilder onIndexUseLeftTrackpadYValue();

	OneDimensionalBindingsBuilder onIndexUseRightTrackpadYValue();

	OneDimensionalBindingsBuilder onIndexUseLeftTrackpadForce();

	OneDimensionalBindingsBuilder onIndexUseRightTrackpadForce();

	OneDimensionalBindingsBuilder onIndexUseLeftTrackpadTouch();

	OneDimensionalBindingsBuilder onIndexUseRightTrackpadTouch();
}