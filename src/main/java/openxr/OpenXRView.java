package openxr;

import static openxr.OpenXRCalls.fill;
import static openxr.OpenXRCalls.toTryCustom;

import java.nio.IntBuffer;
import java.util.function.Consumer;
import java.util.function.IntFunction;
import java.util.function.ToIntBiFunction;

import io.vavr.Tuple2;

import org.joml.Quaternionf;
import org.joml.Quaternionfc;
import org.joml.Vector3f;
import org.joml.Vector3fc;
import org.lwjgl.openxr.KHROpenGLEnable;
import org.lwjgl.openxr.XR10;
import org.lwjgl.openxr.XrCompositionLayerProjectionView;
import org.lwjgl.openxr.XrExtent2Di;
import org.lwjgl.openxr.XrOffset2Di;
import org.lwjgl.openxr.XrPosef;
import org.lwjgl.openxr.XrRect2Di;
import org.lwjgl.openxr.XrSwapchain;
import org.lwjgl.openxr.XrSwapchainImageAcquireInfo;
import org.lwjgl.openxr.XrSwapchainImageBaseHeader;
import org.lwjgl.openxr.XrSwapchainImageOpenGLKHR;
import org.lwjgl.openxr.XrSwapchainImageReleaseInfo;
import org.lwjgl.openxr.XrSwapchainImageWaitInfo;
import org.lwjgl.openxr.XrSwapchainSubImage;
import org.lwjgl.openxr.XrView;
import org.lwjgl.system.MemoryUtil;

public class OpenXRView {
	private static final String ERR_ENUM_SWAPCHAIN_IMAGES = "Failed to enumerate swapchain images";

	private XrView xrView;
	private XrCompositionLayerProjectionView xrCLPView;
	private XrSwapchain xrSwapchain;
	private int width;
	private int height;

	OpenXRView(XrView xrView, XrCompositionLayerProjectionView xrCLPView, XrSwapchain xrSwapchain, int width,
		int height) {

		this.xrView = xrView;
		this.xrCLPView = xrCLPView;
		this.xrSwapchain = xrSwapchain;
		this.width = width;
		this.height = height;
		enumSwapChainImages();

		Consumer<XrOffset2Di> offsetConsumer = offset -> offset.x(0).y(0);
		Consumer<XrExtent2Di> extendConsumer = extent -> extent.width(width).height(height);
		Consumer<XrRect2Di> rectConsumer = rect -> rect.offset(offsetConsumer).extent(extendConsumer);
		Consumer<XrSwapchainSubImage> subImageConsumer = si -> si.swapchain(xrSwapchain).imageRect(rectConsumer);
		this.xrCLPView.subImage(subImageConsumer);
	}

	public void _OpenXRView_() {
		MemoryUtil.memFree(imageIndex);
		xrImageAquireInfo.free();
		xrImageWaitInfo.free();
		xrImageReleaseInfo.free();
		if (images != null) {
			images.free();
			images = null;
		}
		XR10.xrDestroySwapchain(xrSwapchain);
		xrSwapchain = null;
	}

	private final ProjectionView projectionView = new ProjectionView();
	private final Vector3f position = new Vector3f();
	private final Quaternionf orientation = new Quaternionf();

	public ProjectionView getProjectionView() {
		return projectionView;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	private XrSwapchainImageOpenGLKHR.Buffer images;

	private void enumSwapChainImages() {
		final ToIntBiFunction<IntBuffer, XrSwapchainImageOpenGLKHR.Buffer> fSCImages = //
			(bCount, bSCImages) -> XR10.xrEnumerateSwapchainImages(xrSwapchain, bCount,
				bSCImages != null ? XrSwapchainImageBaseHeader.create(bSCImages) : null);
		final IntFunction<XrSwapchainImageOpenGLKHR.Buffer> bufferCreator = count -> {
			return fill(XrSwapchainImageOpenGLKHR.calloc(count), XrSwapchainImageOpenGLKHR.TYPE,
				KHROpenGLEnable.XR_TYPE_SWAPCHAIN_IMAGE_OPENGL_KHR);
		};

		images = toTryCustom(bufferCreator, fSCImages, ERR_ENUM_SWAPCHAIN_IMAGES).mapTry(Tuple2::_2).get();
	}

	public int getImageCount() {
		return images.capacity();
	}

	private final IntBuffer imageIndex = MemoryUtil.memAllocInt(1);
	private final XrSwapchainImageAcquireInfo xrImageAquireInfo = XrSwapchainImageAcquireInfo.calloc().type$Default();
	private final XrSwapchainImageWaitInfo xrImageWaitInfo = XrSwapchainImageWaitInfo.malloc()
		.type$Default()
		.next(MemoryUtil.NULL)
		.timeout(XR10.XR_INFINITE_DURATION);
	private final XrSwapchainImageReleaseInfo xrImageReleaseInfo = XrSwapchainImageReleaseInfo.calloc().type$Default();

	public void aquireAndWaitImage() {
		XR10.xrAcquireSwapchainImage(xrSwapchain, xrImageAquireInfo, imageIndex);
		XR10.xrWaitSwapchainImage(xrSwapchain, xrImageWaitInfo);
	}

	public int getImageIndex() {
		return imageIndex.get(0);
	}

	public int getCurrentTexture() {
		return getTexture(getImageIndex());
	}

	public int getTexture(int index) {
		return images.get(index).image();
	}

	public void releaseImage() {
		XR10.xrReleaseSwapchainImage(xrSwapchain, xrImageReleaseInfo);
	}

	public void updateLayerView() {
		final XrPosef pose = xrView.pose();
		xrCLPView.pose(pose).fov(xrView.fov());
		position.set(pose.position$().x(), pose.position$().y(), pose.position$().z());
		orientation.set(pose.orientation().x(), pose.orientation().y(), pose.orientation().z(), pose.orientation().w());
	}

	public final class ProjectionView {
		private ProjectionView() {
		}

		public int getSubImageOffsetX() {
			return xrCLPView.subImage().imageRect().offset().x();
		}

		public int getSubImageOffsetY() {
			return xrCLPView.subImage().imageRect().offset().y();
		}

		public int getSubImageWidth() {
			return xrCLPView.subImage().imageRect().extent().width();
		}

		public int getSubImageHeight() {
			return xrCLPView.subImage().imageRect().extent().height();
		}

		public float getFoVAngleLeft() {
			return xrCLPView.fov().angleLeft();
		}

		public float getFoVAngleRight() {
			return xrCLPView.fov().angleRight();
		}

		public float getFoVAngleUp() {
			return xrCLPView.fov().angleUp();
		}

		public float getFoVAngleDown() {
			return xrCLPView.fov().angleDown();
		}

		public Vector3fc getPosition() {
			return position;
		}

		public Quaternionfc getOrientation() {
			return orientation;
		}
	}
}
