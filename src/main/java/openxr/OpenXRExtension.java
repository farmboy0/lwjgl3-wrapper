package openxr;

import java.util.Optional;

import io.vavr.API;
import io.vavr.collection.Seq;
import io.vavr.collection.Set;

public enum OpenXRExtension {
	XR_ALMALENCE_DIGITAL_LENS_CONTROL("XR_ALMALENCE_digital_lens_control"), //

	XR_BD_BODY_TRACKING("XR_BD_body_tracking"), //
	XR_BD_CONTROLLER_INTERACTION("XR_BD_controller_interaction"), //

	XR_EPIC_VIEW_CONFIGURATION_FOV("XR_EPIC_view_configuration_fov"), //

	XR_EXT_ACTIVE_ACTION_SET_PRIORITY("XR_EXT_active_action_set_priority"), //
	XR_EXT_COMPOSITION_LAYER_INVERTED_ALPHA("XR_EXT_composition_layer_inverted_alpha"), //
	XR_EXT_CONFORMANCE_AUTOMATION("XR_EXT_conformance_automation"), //
	XR_EXT_CONTROLLER_MODEL("XR_EXT_controller_model"), //
	XR_EXT_DEBUG_UTILS("XR_EXT_debug_utils"), //
	XR_EXT_DPAD_BINDING("XR_EXT_dpad_binding"), //
	XR_EXT_EYE_GAZE_INTERACTION("XR_EXT_eye_gaze_interaction"), //
	XR_EXT_FRAME_SYNTHESIS("XR_EXT_frame_synthesis"), //
	XR_EXT_FUTURE("XR_EXT_future"), //
	XR_EXT_HAND_INTERACTION("XR_EXT_hand_interaction"), //
	XR_EXT_HAND_JOINTS_MOTION_RANGE("XR_EXT_hand_joints_motion_range"), //
	XR_EXT_HAND_TRACKING_DATA_SOURCE("XR_EXT_hand_tracking_data_source"), //
	XR_EXT_HAND_TRACKING("XR_EXT_hand_tracking"), //
	XR_EXT_HAPTIC_PARAMETRIC("XR_EXT_haptic_parametric"), //
	XR_EXT_HP_MIXED_REALITY_CONTROLLER("XR_EXT_hp_mixed_reality_controller"), //
	XR_EXT_LOCAL_FLOOR("XR_EXT_local_floor"), //
	XR_EXT_PALM_POSE("XR_EXT_palm_pose"), //
	XR_EXT_PERFORMANCE_SETTINGS("XR_EXT_performance_settings"), //
	XR_EXT_PERMISSIONS_SUPPORT("XR_EXT_permissions_support"), //
	XR_EXT_PLANE_DETECTION("XR_EXT_plane_detection"), //
	XR_EXT_RENDER_MODEL("XR_EXT_render_model"), //
	XR_EXT_SAMSUNG_ODYSSEY_CONTROLLER("XR_EXT_samsung_odyssey_controller"), //
	XR_EXT_SPATIAL_ANCHOR("XR_EXT_spatial_anchor"), //
	XR_EXT_SPATIAL_DISCOVERY_BOUNDS("XR_EXT_spatial_discovery_bounds"), //
	XR_EXT_SPATIAL_DISCOVERY_RAYCAST("XR_EXT_spatial_discovery_raycast"), //
	XR_EXT_SPATIAL_ENTITY("XR_EXT_spatial_entity"), //
	XR_EXT_SPATIAL_MARKER_TRACKING("XR_EXT_spatial_marker_tracking"), //
	XR_EXT_SPATIAL_PERSISTENCE("XR_EXT_spatial_persistence"), //
	XR_EXT_SPATIAL_PLANE_TRACKING("XR_EXT_spatial_plane_tracking"), //
	XR_EXT_THERMAL_QUERY("XR_EXT_thermal_query"), //
	XR_EXT_USER_PRESENCE("XR_EXT_user_presence"), //
	XR_EXT_UUID("XR_EXT_uuid"), //
	XR_EXT_VIEW_CONFIGURATION_DEPTH_RANGE("XR_EXT_view_configuration_depth_range"), //
	XR_EXT_WIN32_APPCONTAINER_COMPATIBLE("XR_EXT_win32_appcontainer_compatible"), //
	XR_EXTX_OVERLAY("XR_EXTX_overlay"), //

	XR_FB_ANDROID_SURFACE_SWAPCHAIN_CREATE("XR_FB_android_surface_swapchain_create"), //
	XR_FB_BODY_TRACKING("XR_FB_body_tracking"), //
	XR_FB_COLOR_SPACE("XR_FB_color_space"), //
	XR_FB_COMPOSITION_LAYER_ALPHA_BLEND("XR_FB_composition_layer_alpha_blend"), //
	XR_FB_COMPOSITION_LAYER_DEPTH_TEST("XR_FB_composition_layer_depth_test"), //
	XR_FB_COMPOSITION_LAYER_IMAGE_LAYOUT("XR_FB_composition_layer_image_layout"), //
	XR_FB_COMPOSITION_LAYER_SECURE_CONTENT("XR_FB_composition_layer_secure_content"), //
	XR_FB_COMPOSITION_LAYER_SETTINGS("XR_FB_composition_layer_settings"), //
	XR_FB_DISPLAY_REFRESH_RATE("XR_FB_display_refresh_rate"), //
	XR_FB_EYE_TRACKING_SOCIAL("XR_FB_eye_tracking_social"), //
	XR_FB_FACE_TRACKING2("XR_FB_face_tracking2"), //
	XR_FB_FACE_TRACKING("XR_FB_face_tracking"), //
	XR_FB_FOVEATION_CONFIGURATION("XR_FB_foveation_configuration"), //
	XR_FB_FOVEATION_VULKAN("XR_FB_foveation_vulkan"), //
	XR_FB_FOVEATION("XR_FB_foveation"), //
	XR_FB_HAND_TRACKING_AIM("XR_FB_hand_tracking_aim"), //
	XR_FB_HAND_TRACKING_CAPSULES("XR_FB_hand_tracking_capsules"), //
	XR_FB_HAND_TRACKING_MESH("XR_FB_hand_tracking_mesh"), //
	XR_FB_HAPTIC_AMPLITUDE_ENVELOPE("XR_FB_haptic_amplitude_envelope"), //
	XR_FB_HAPTIC_PCM("XR_FB_haptic_pcm"), //
	XR_FB_KEYBOARD_TRACKING("XR_FB_keyboard_tracking"), //
	XR_FB_PASSTHROUGH_KEYBOARD_HANDS("XR_FB_passthrough_keyboard_hands"), //
	XR_FB_PASSTHROUGH("XR_FB_passthrough"), //
	XR_FB_RENDER_MODEL("XR_FB_render_model"), //
	XR_FB_SCENE_CAPTURE("XR_FB_scene_capture"), //
	XR_FB_SCENE("XR_FB_scene"), //
	XR_FB_SPACE_WARP("XR_FB_space_warp"), //
	XR_FB_SPATIAL_ENTITY_CONTAINER("XR_FB_spatial_entity_container"), //
	XR_FB_SPATIAL_ENTITY_QUERY("XR_FB_spatial_entity_query"), //
	XR_FB_SPATIAL_ENTITY_SHARING("XR_FB_spatial_entity_sharing"), //
	XR_FB_SPATIAL_ENTITY_STORAGE_BATCH("XR_FB_spatial_entity_storage_batch"), //
	XR_FB_SPATIAL_ENTITY_STORAGE("XR_FB_spatial_entity_storage"), //
	XR_FB_SPATIAL_ENTITY_USER("XR_FB_spatial_entity_user"), //
	XR_FB_SPATIAL_ENTITY("XR_FB_spatial_entity"), //
	XR_FB_SWAPCHAIN_UPDATE_STATE_ANDROID_SURFACE("XR_FB_swapchain_update_state_android_surface"), //
	XR_FB_SWAPCHAIN_UPDATE_STATE_OPENGL_ES("XR_FB_swapchain_update_state_opengl_es"), //
	XR_FB_SWAPCHAIN_UPDATE_STATE_VULKAN("XR_FB_swapchain_update_state_vulkan"), //
	XR_FB_SWAPCHAIN_UPDATE_STATE("XR_FB_swapchain_update_state"), //
	XR_FB_TOUCH_CONTROLLER_PROXIMITY("XR_FB_touch_controller_proximity"), //
	XR_FB_TOUCH_CONTROLLER_PRO("XR_FB_touch_controller_pro"), //
	XR_FB_TRIANGLE_MESH("XR_FB_triangle_mesh"), //

	XR_HTC_ANCHOR("XR_HTC_anchor"), //
	XR_HTC_BODY_TRACKING("XR_HTC_body_tracking"), //
	XR_HTC_FACIAL_TRACKING("XR_HTC_facial_tracking"), //
	XR_HTC_FOVEATION("XR_HTC_foveation"), //
	XR_HTC_HAND_INTERACTION("XR_HTC_hand_interaction"), //
	XR_HTC_PASSTHROUGH("XR_HTC_passthrough"), //
	XR_HTC_VIVE_COSMOS_CONTROLLER_INTERACTION("XR_HTC_vive_cosmos_controller_interaction"), //
	XR_HTC_VIVE_FOCUS3_CONTROLLER_INTERACTION("XR_HTC_vive_focus3_controller_interaction"), //
	XR_HTC_VIVE_WRIST_TRACKER_INTERACTION("XR_HTC_vive_wrist_tracker_interaction"), //
	XR_HTCX_VIVE_TRACKER_INTERACTION("XR_HTCX_vive_tracker_interaction"), //

	XR_HUAWEI_6DOF_CONTROLLER_INTERACTION("XR_HUAWEI_6dof_controller_interaction"), //
	XR_HUAWEI_CONTROLLER_INTERACTION("XR_HUAWEI_controller_interaction"), //

	XR_KHR_ANDROID_CREATE_INSTANCE("XR_KHR_android_create_instance"), //
	XR_KHR_ANDROID_SURFACE_SWAPCHAIN("XR_KHR_android_surface_swapchain"), //
	XR_KHR_ANDROID_THREAD_SETTINGS("XR_KHR_android_thread_settings"), //
	XR_KHR_BINDING_MODIFICATION("XR_KHR_binding_modification"), //
	XR_KHR_COMPOSITION_LAYER_COLOR_SCALE_BIAS("XR_KHR_composition_layer_color_scale_bias"), //
	XR_KHR_COMPOSITION_LAYER_CUBE("XR_KHR_composition_layer_cube"), //
	XR_KHR_COMPOSITION_LAYER_CYLINDER("XR_KHR_composition_layer_cylinder"), //
	XR_KHR_COMPOSITION_LAYER_DEPTH("XR_KHR_composition_layer_depth"), //
	XR_KHR_COMPOSITION_LAYER_EQUIRECT2("XR_KHR_composition_layer_equirect2"), //
	XR_KHR_COMPOSITION_LAYER_EQUIRECT("XR_KHR_composition_layer_equirect"), //
	XR_KHR_CONVERT_TIMESPEC_TIME("XR_KHR_convert_timespec_time"), //
	XR_KHR_D3D10_ENABLE_OBSOLETE("XR_KHR_D3D10_enable_obsolete"), //
	XR_KHR_D3D11_ENABLE("XR_KHR_D3D11_enable"), //
	XR_KHR_D3D12_ENABLE("XR_KHR_D3D12_enable"), //
	XR_KHR_EGL_ENABLE("XR_KHR_egl_enable"), //
	XR_KHR_EXTENDABLE_ACTION_BINDING("XR_KHR_extendable_action_binding"), //
	XR_KHR_EXTENDED_STRUCT_NAME_LENGTHS("XR_KHR_extended_struct_name_lengths"), //
	XR_KHR_GAME_CONTROLLER("XR_KHR_game_controller"), //
	XR_KHR_HAND_TRACKING("XR_KHR_hand_tracking"), //
	XR_KHR_HEADLESS("XR_KHR_headless"), //
	XR_KHR_LOADER_INIT_ANDROID("XR_KHR_loader_init_android"), //
	XR_KHR_LOADER_INIT("XR_KHR_loader_init"), //
	XR_KHR_LOCATE_SPACES("XR_KHR_locate_spaces"), //
	XR_KHR_MAINTENANCE1("XR_KHR_maintenance1"), //
	XR_KHR_METAL_ENABLE("XR_KHR_metal_enable"), //
	XR_KHR_OPENGL_ENABLE("XR_KHR_opengl_enable"), //
	XR_KHR_OPENGL_ES_ENABLE("XR_KHR_opengl_es_enable"), //
	XR_KHR_OVERLAYS("XR_KHR_overlays"), //
	XR_KHR_SWAPCHAIN_USAGE_INPUT_ATTACHMENT_BIT("XR_KHR_swapchain_usage_input_attachment_bit"), //
	XR_KHR_VARIABLE_RATE_RENDERING("XR_KHR_variable_rate_rendering"), //
	XR_KHR_VISIBILITY_MASK("XR_KHR_visibility_mask"), //
	XR_KHR_VULKAN_ENABLE2("XR_KHR_vulkan_enable2"), //
	XR_KHR_VULKAN_ENABLE("XR_KHR_vulkan_enable"), //
	XR_KHR_VULKAN_SWAPCHAIN_FORMAT_LIST("XR_KHR_vulkan_swapchain_format_list"), //
	XR_KHR_WIN32_CONVERT_PERFORMANCE_COUNTER_TIME("XR_KHR_win32_convert_performance_counter_time"), //

	XR_LOGITECH_MX_INK_STYLUS_INTERACTION("XR_LOGITECH_mx_ink_stylus_interaction"), //

	XR_META_AUTOMATIC_LAYER_FILTER("XR_META_automatic_layer_filter"), //
	XR_META_COLOCATION_DISCOVERY("XR_META_colocation_discovery"), //
	XR_META_ENVIRONMENT_DEPTH("XR_META_environment_depth"), //
	XR_META_FOVEATION_EYE_TRACKED("XR_META_foveation_eye_tracked"), //
	XR_META_HAND_TRACKING_MICROGESTURES("XR_META_hand_tracking_microgestures"), //
	XR_META_HEADSET_ID("XR_META_headset_id"), //
	XR_META_LOCAL_DIMMING("XR_META_local_dimming"), //
	XR_META_PASSTHROUGH_COLOR_LUT("XR_META_passthrough_color_lut"), //
	XR_META_PASSTHROUGH_LAYER_RESUMED_EVENT("XR_META_passthrough_layer_resumed_event"), //
	XR_META_PASSTHROUGH_PREFERENCES("XR_META_passthrough_preferences"), //
	XR_META_PERFORMANCE_METRICS("XR_META_performance_metrics"), //
	XR_META_RECOMMENDED_LAYER_RESOLUTION("XR_META_recommended_layer_resolution"), //
	XR_META_SPATIAL_ENTITY_GROUP_SHARING("XR_META_spatial_entity_group_sharing"), //
	XR_META_SPATIAL_ENTITY_MESH("XR_META_spatial_entity_mesh"), //
	XR_META_SPATIAL_ENTITY_SHARING("XR_META_spatial_entity_sharing"), //
	XR_META_TOUCH_CONTROLLER_PLUS("XR_META_touch_controller_plus"), //
	XR_META_VIRTUAL_KEYBOARD("XR_META_virtual_keyboard"), //
	XR_META_VULKAN_SWAPCHAIN_CREATE_INFO("XR_META_vulkan_swapchain_create_info"), //

	XR_ML_COMPAT("XR_ML_compat"), //
	XR_ML_FACIAL_EXPRESSION("XR_ML_facial_expression"), //
	XR_ML_FRAME_END_INFO("XR_ML_frame_end_info"), //
	XR_ML_GLOBAL_DIMMER("XR_ML_global_dimmer"), //
	XR_ML_LOCALIZATION_MAP("XR_ML_localization_map"), //
	XR_ML_MARKER_UNDERSTANDING("XR_ML_marker_understanding"), //
	XR_ML_ML2_CONTROLLER_INTERACTION("XR_ML_ml2_controller_interaction"), //
	XR_ML_SPATIAL_ANCHORS_STORAGE("XR_ML_spatial_anchors_storage"), //
	XR_ML_SPATIAL_ANCHORS("XR_ML_spatial_anchors"), //
	XR_ML_SYSTEM_NOTIFICATIONS("XR_ML_system_notifications"), //
	XR_ML_USER_CALIBRATION("XR_ML_user_calibration"), //
	XR_ML_VIEW_CONFIGURATION_DEPTH_RANGE_CHANGE("XR_ML_view_configuration_depth_range_change"), //
	XR_ML_WORLD_MESH_DETECTION("XR_ML_world_mesh_detection"), //

	XR_MND_HEADLESS("XR_MND_headless"), //
	XR_MND_SWAPCHAIN_USAGE_INPUT_ATTACHMENT_BIT("XR_MND_swapchain_usage_input_attachment_bit"), //
	XR_MNDX_EGL_ENABLE("XR_MNDX_egl_enable"), //
	XR_MNDX_FORCE_FEEDBACK_CURL("XR_MNDX_force_feedback_curl"), //

	XR_MSFT_COMPOSITION_LAYER_REPROJECTION("XR_MSFT_composition_layer_reprojection"), //
	XR_MSFT_CONTROLLER_MODEL("XR_MSFT_controller_model"), //
	XR_MSFT_FIRST_PERSON_OBSERVER("XR_MSFT_first_person_observer"), //
	XR_MSFT_HAND_INTERACTION("XR_MSFT_hand_interaction"), //
	XR_MSFT_HAND_TRACKING_MESH("XR_MSFT_hand_tracking_mesh"), //
	XR_MSFT_HOLOGRAPHIC_WINDOW_ATTACHMENT("XR_MSFT_holographic_window_attachment"), //
	XR_MSFT_PERCEPTION_ANCHOR_INTEROP("XR_MSFT_perception_anchor_interop"), //
	XR_MSFT_SCENE_MARKER("XR_MSFT_scene_marker"), //
	XR_MSFT_SCENE_UNDERSTANDING_SERIALIZATION("XR_MSFT_scene_understanding_serialization"), //
	XR_MSFT_SCENE_UNDERSTANDING("XR_MSFT_scene_understanding"), //
	XR_MSFT_SECONDARY_VIEW_CONFIGURATION("XR_MSFT_secondary_view_configuration"), //
	XR_MSFT_SPATIAL_ANCHOR_PERSISTENCE("XR_MSFT_spatial_anchor_persistence"), //
	XR_MSFT_SPATIAL_ANCHOR("XR_MSFT_spatial_anchor"), //
	XR_MSFT_SPATIAL_GRAPH_BRIDGE("XR_MSFT_spatial_graph_bridge"), //
	XR_MSFT_UNBOUNDED_REFERENCE_SPACE("XR_MSFT_unbounded_reference_space"), //

	XR_OCULUS_ANDROID_SESSION_STATE_ENABLE("XR_OCULUS_android_session_state_enable"), //
	XR_OCULUS_AUDIO_DEVICE_GUID("XR_OCULUS_audio_device_guid"), //
	XR_OCULUS_EXTERNAL_CAMERA("XR_OCULUS_external_camera"), //

	XR_OPPO_CONTROLLER_INTERACTION("XR_OPPO_controller_interaction"), //

	XR_QCOM_IMAGE_TRACKING("XR_QCOM_image_tracking"), //
	XR_QCOM_OBJECT_TRACKING("XR_QCOM_object_tracking"), //
	XR_QCOM_PLANE_DETECTION("XR_QCOM_plane_detection"), //
	XR_QCOM_RAY_CASTING("XR_QCOM_ray_casting"), //
	XR_QCOM_TRACKING_OPTIMIZATION_SETTINGS("XR_QCOM_tracking_optimization_settings"), //

	XR_ULTRALEAP_HAND_TRACKING_FOREARM("XR_ULTRALEAP_hand_tracking_forearm"), //

	XR_VALVE_ANALOG_THRESHOLD("XR_VALVE_analog_threshold"), //

	XR_VARJO_COMPOSITION_LAYER_DEPTH_TEST("XR_VARJO_composition_layer_depth_test"), //
	XR_VARJO_ENVIRONMENT_DEPTH_ESTIMATION("XR_VARJO_environment_depth_estimation"), //
	XR_VARJO_FOVEATED_RENDERING("XR_VARJO_foveated_rendering"), //
	XR_VARJO_MARKER_TRACKING("XR_VARJO_marker_tracking"), //
	XR_VARJO_QUAD_VIEWS("XR_VARJO_quad_views"), //
	XR_VARJO_VIEW_OFFSET("XR_VARJO_view_offset"), //
	XR_VARJO_XR4_CONTROLLER_INTERACTION("XR_VARJO_xr4_controller_interaction"), //

	XR_YVR_CONTROLLER_INTERACTION("XR_YVR_controller_interaction"), //
	;

	public static final Seq<String> EXTENSIONS = OpenXR1_0.enumInstanceExtensions();

	private final String name;

	private OpenXRExtension(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public boolean isAvailable() {
		return EXTENSIONS.contains(name);
	}

	public void ifAvailable(Runnable available) {
		if (isAvailable())
			available.run();
	}

	public void ifAvailable(Runnable available, Runnable notAvailable) {
		if (isAvailable())
			available.run();
		else
			notAvailable.run();
	}

	private OpenXRException unavailable() {
		return new OpenXRException(String.format("Extension %s is not available.", getName()));
	}

	public Optional<OpenXRExtension> toOptional() {
		return isAvailable() ? Optional.of(this) : Optional.empty();
	}

	public static OpenXRExtensionBuilder builder() {
		return new OpenXRExtensionBuilder();
	}

	public static class OpenXRExtensionBuilder {
		private Set<OpenXRExtension> extensions = API.Set();

		private OpenXRExtensionBuilder() {
		}

		public OpenXRExtensionBuilder require(OpenXRExtension extension) throws OpenXRException {
			if (!extension.isAvailable())
				throw extension.unavailable();
			extensions = extensions.add(extension);
			return this;
		}

		public OpenXRExtensionBuilder useIfAvailable(OpenXRExtension extension) {
			if (extension.isAvailable())
				extensions = extensions.add(extension);
			return this;
		}

		public OpenXRExtensionBuilder preferOne(OpenXRExtension extension, OpenXRExtension... others)
			throws OpenXRException {

			OpenXRExtension found = API.Seq(others)
				.prepend(extension)
				.find(OpenXRExtension::isAvailable)
				.getOrElseThrow(() -> new OpenXRException(preferMsg(extension, others)));
			extensions = extensions.add(found);
			return this;
		}

		private String preferMsg(OpenXRExtension extension, OpenXRExtension... others) {
			return API.Seq(others)
				.prepend(extension)
				.map(OpenXRExtension::getName)
				.mkString("None of the extensions", ", ", "is available.");
		}

		public Seq<String> build() {
			return extensions.toArray().map(OpenXRExtension::getName);
		}
	}
}
