package openxr;

import io.vavr.API;
import io.vavr.collection.Seq;

public interface OpenXRDebugListener {
	int invoke(Severity severity, Seq<Channel> channel, String messageId, String functionName, String message,
		Seq<String> labels);

	enum Severity {
		VERBOSE(0x1), INFO(0x10), WARNING(0x100), ERROR(0x1000);

		private final int xrVal;

		private Severity(int xrVal) {
			this.xrVal = xrVal;
		}

		public static final Severity from(long value) {
			return API.Seq(values()).find(s -> s.xrVal == value).getOrNull();
		}
	}

	enum Channel {
		GENERAL(0x1), VALIDATION(0x2), PERFORMANCE(0x4), CONFORMANCE(0x8);

		private final int xrVal;

		private Channel(int xrVal) {
			this.xrVal = xrVal;
		}

		public static final Seq<Channel> from(long value) {
			return API.Seq(values()).filter(s -> (s.xrVal & value) > 0);
		}
	}
}
