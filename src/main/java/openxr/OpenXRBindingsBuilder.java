package openxr;

import javax.annotation.Nonnull;

public interface OpenXRBindingsBuilder {
	OneDimensionalBindingsBuilder addBooleanAction(@Nonnull String id, @Nonnull String description);

	OneDimensionalBindingsBuilder addFloatAction(@Nonnull String id, @Nonnull String description);

	TwoDimensionalBindingsBuilder addVector2fAction(@Nonnull String id, @Nonnull String description);

	PoseBindingsBuilder addPoseAction(@Nonnull String id, @Nonnull String description, @Nonnull OpenXRSession session);

	OpenXRActionSet suggestBindings();
}
