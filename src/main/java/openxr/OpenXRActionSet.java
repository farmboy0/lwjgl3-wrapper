package openxr;

import static openxr.OpenXRCalls.toTryStack;

import java.nio.LongBuffer;
import java.util.function.ToIntFunction;

import javax.annotation.Nonnull;
import javax.annotation.OverridingMethodsMustInvokeSuper;

import io.vavr.API;
import io.vavr.collection.Map;
import io.vavr.collection.Seq;
import io.vavr.collection.Set;

import org.joml.Quaternionf;
import org.joml.Quaternionfc;
import org.joml.Vector2f;
import org.joml.Vector2fc;
import org.joml.Vector3f;
import org.joml.Vector3fc;
import org.lwjgl.PointerBuffer;
import org.lwjgl.openxr.XR10;
import org.lwjgl.openxr.XrAction;
import org.lwjgl.openxr.XrActionCreateInfo;
import org.lwjgl.openxr.XrActionSet;
import org.lwjgl.openxr.XrActionStateBoolean;
import org.lwjgl.openxr.XrActionStateFloat;
import org.lwjgl.openxr.XrActionStateGetInfo;
import org.lwjgl.openxr.XrActionStatePose;
import org.lwjgl.openxr.XrActionStateVector2f;
import org.lwjgl.openxr.XrActionSuggestedBinding;
import org.lwjgl.openxr.XrInteractionProfileSuggestedBinding;
import org.lwjgl.openxr.XrPosef;
import org.lwjgl.openxr.XrQuaternionf;
import org.lwjgl.openxr.XrSpace;
import org.lwjgl.openxr.XrSpaceLocation;
import org.lwjgl.openxr.XrVector3f;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.MemoryUtil;

public class OpenXRActionSet {
	private static final String ERR_CREATE_ACTION = "Failed to create action";
	private static final String ERR_STRING_TO_PATH = "Failed to convert string to path";
	private static final String ERR_SUGGEST_BINDINGS = "Failed to suggest bindings";

	XrActionSet xrActionSet;

	private Map<String, OpenXRAction> actions = API.Map();

	OpenXRActionSet(XrActionSet xrActionSet) {
		this.xrActionSet = xrActionSet;
	}

	public void _OpenXRActionSet_() {
		actions.values().forEach(OpenXRAction::_OpenXRAction_);
		actions = API.Map();
		XR10.xrDestroyActionSet(xrActionSet);
		xrActionSet = null;
	}

	void pollActions(@Nonnull OpenXRSession session) {
		actions.values().forEach(action -> action.poll(session));
	}

	void pollPoses(@Nonnull OpenXRSession session, long time) {
		actions.values()
			.filter(PoseAction.class::isInstance)
			.map(PoseAction.class::cast)
			.forEach(action -> action.poll(session, time));
	}

	public OneDimensionalBindingsBuilder addBooleanAction(@Nonnull String id, @Nonnull String description) {
		final OpenXRAction action = new BooleanAction(createAction(id, description, XR10.XR_ACTION_TYPE_BOOLEAN_INPUT));
		actions = actions.put(id, action);
		return new OpenXRBindingsBuilderImpl(this, action);
	}

	public OneDimensionalBindingsBuilder addFloatAction(@Nonnull String id, @Nonnull String description) {
		final OpenXRAction action = new FloatAction(createAction(id, description, XR10.XR_ACTION_TYPE_FLOAT_INPUT));
		actions = actions.put(id, action);
		return new OpenXRBindingsBuilderImpl(this, action);
	}

	public TwoDimensionalBindingsBuilder addVector2fAction(@Nonnull String id, @Nonnull String description) {
		final OpenXRAction action = new Vector2fAction(
			createAction(id, description, XR10.XR_ACTION_TYPE_VECTOR2F_INPUT));
		actions = actions.put(id, action);
		return new OpenXRBindingsBuilderImpl(this, action);
	}

	public PoseBindingsBuilder addPoseAction(@Nonnull String id, @Nonnull String description,
		@Nonnull OpenXRSession session) {

		final XrAction xrAction = createAction(id, description, XR10.XR_ACTION_TYPE_POSE_INPUT);
		final XrSpace xrActionSpace = session.createActionSpace(xrAction);
		final OpenXRAction action = new PoseAction(xrAction, xrActionSpace);
		actions = actions.put(id, action);
		return new OpenXRBindingsBuilderImpl(this, action);
	}

	private XrAction createAction(String id, String description, int type) {
		try (MemoryStack stack = MemoryStack.stackPush()) {
			final XrActionCreateInfo actionCI = XrActionCreateInfo.calloc(stack)
				.type$Default()
				.next(MemoryUtil.NULL)
				.actionType(type)
				.actionName(stack.UTF8(id))
				.localizedActionName(stack.UTF8(description));

			final ToIntFunction<PointerBuffer> fAction = bAction -> XR10.xrCreateAction(xrActionSet, actionCI, bAction);
			return toTryStack(stack.mallocPointer(1), fAction, ERR_CREATE_ACTION).mapTry(bAction -> {
				return new XrAction(bAction.get(0), xrActionSet);
			}).get();
		}
	}

	public openxr.OpenXRActionSet.BooleanAction.BooleanState getBooleanState(@Nonnull String id) {
		return actions.get(id)
			.toJavaOptional()
			.filter(BooleanAction.class::isInstance)
			.map(BooleanAction.class::cast)
			.map(BooleanAction::getState)
			.orElseThrow(OpenXRException::new);
	}

	public openxr.OpenXRActionSet.FloatAction.FloatState getFloatState(@Nonnull String id) {
		return actions.get(id)
			.toJavaOptional()
			.filter(FloatAction.class::isInstance)
			.map(FloatAction.class::cast)
			.map(FloatAction::getState)
			.orElseThrow(OpenXRException::new);
	}

	public openxr.OpenXRActionSet.Vector2fAction.Vector2fState getVector2fState(@Nonnull String id) {
		return actions.get(id)
			.toJavaOptional()
			.filter(Vector2fAction.class::isInstance)
			.map(Vector2fAction.class::cast)
			.map(Vector2fAction::getState)
			.orElseThrow(OpenXRException::new);
	}

	public openxr.OpenXRActionSet.PoseAction.PoseState getPoseState(@Nonnull String id) {
		return actions.get(id)
			.toJavaOptional()
			.filter(PoseAction.class::isInstance)
			.map(PoseAction.class::cast)
			.map(PoseAction::getState)
			.orElseThrow(OpenXRException::new);
	}

	public void suggestBindings() {
		actions.values().flatMap(OpenXRAction::getProfiles).toSet().forEach(profile -> {
			suggestBindings(profile, actions.values().flatMap(action -> action.getBindings(profile)));
		});
	}

	private void suggestBindings(String profile, Seq<ActionBinding> bindingsSet) {
		try (MemoryStack stack = MemoryStack.stackPush()) {
			final XrActionSuggestedBinding.Buffer buffer = XrActionSuggestedBinding.calloc(bindingsSet.size(), stack);
			bindingsSet.forEachWithIndex((actionBinding, i) -> fillBindingBuffer(buffer, i, actionBinding, stack));
			final XrInteractionProfileSuggestedBinding bindings = XrInteractionProfileSuggestedBinding.calloc(stack)
				.type$Default()
				.next(MemoryUtil.NULL)
				.interactionProfile(toPath(stack, profile))
				.suggestedBindings(buffer);
			int result = XR10.xrSuggestInteractionProfileBindings(xrActionSet.getInstance(), bindings);
			if (!XR10.XR_SUCCEEDED(result)) {
				throw new OpenXRError(result, ERR_SUGGEST_BINDINGS);
			}
		}
	}

	private void fillBindingBuffer(XrActionSuggestedBinding.Buffer buffer, int i, ActionBinding ab, MemoryStack stack) {
		buffer.get(i).action(ab.xrAction()).binding(toPath(stack, ab.binding()));
	}

	private long toPath(MemoryStack stack, String path) {
		final ToIntFunction<LongBuffer> fPath = bPath -> XR10.xrStringToPath(xrActionSet.getInstance(), path, bPath);
		return toTryStack(stack.mallocLong(1), fPath, ERR_STRING_TO_PATH).mapTry(bPath -> {
			return bPath.get(0);
		}).get();
	}

	abstract class OpenXRAction {
		private final XrAction xrAction;
		private Map<String, Set<String>> suggestedBindings = API.Map();

		OpenXRAction(XrAction xrAction) {
			this.xrAction = xrAction;
		}

		@OverridingMethodsMustInvokeSuper
		void _OpenXRAction_() {
			XR10.xrDestroyAction(xrAction);
		}

		abstract void poll(OpenXRSession session);

		Set<String> getProfiles() {
			return suggestedBindings.keySet();
		}

		Set<ActionBinding> getBindings(String profile) {
			return suggestedBindings.get(profile)
				.getOrElse(API::Set)
				.map(binding -> new ActionBinding(xrAction, binding));
		}

		void addBinding(String profile, String input) {
			suggestedBindings = suggestedBindings.put(profile,
				suggestedBindings.getOrElse(profile, API.Set()).add(input));
		}
	}

	public final class BooleanAction extends OpenXRAction {
		private final XrActionStateBoolean xrState = XrActionStateBoolean.calloc().type$Default();
		private final XrActionStateGetInfo actionStateGI;
		private final BooleanState state = new BooleanState();

		private BooleanAction(XrAction xrAction) {
			super(xrAction);
			actionStateGI = XrActionStateGetInfo.calloc()
				.type$Default()
				.action(xrAction)
				.subactionPath(XR10.XR_NULL_PATH);
		}

		@Override
		void _OpenXRAction_() {
			super._OpenXRAction_();
			xrState.free();
			actionStateGI.free();
		}

		@Override
		void poll(OpenXRSession session) {
			session.pollBooleanAction(actionStateGI, xrState);
		}

		private BooleanState getState() {
			return state;
		}

		public final class BooleanState {
			public boolean currentState() {
				return xrState.currentState();
			}

			public boolean changedSinceLastSync() {
				return xrState.changedSinceLastSync();
			}

			public long lastChangeTime() {
				return xrState.lastChangeTime();
			}

			public boolean isActive() {
				return xrState.isActive();
			}
		}
	}

	public final class FloatAction extends OpenXRAction {
		private final XrActionStateFloat xrState = XrActionStateFloat.calloc().type$Default();
		private final XrActionStateGetInfo actionStateGI;
		private final FloatState state = new FloatState();

		private FloatAction(XrAction xrAction) {
			super(xrAction);
			actionStateGI = XrActionStateGetInfo.calloc().type$Default().action(xrAction);
		}

		@Override
		void _OpenXRAction_() {
			super._OpenXRAction_();
			xrState.free();
			actionStateGI.free();
		}

		@Override
		void poll(OpenXRSession session) {
			session.pollFloatAction(actionStateGI, xrState);
		}

		private FloatState getState() {
			return state;
		}

		public final class FloatState {
			public float currentState() {
				return xrState.currentState();
			}

			public boolean changedSinceLastSync() {
				return xrState.changedSinceLastSync();
			}

			public long lastChangeTime() {
				return xrState.lastChangeTime();
			}

			public boolean isActive() {
				return xrState.isActive();
			}
		}
	}

	public final class Vector2fAction extends OpenXRAction {
		private final XrActionStateVector2f xrState = XrActionStateVector2f.calloc().type$Default();
		private final XrActionStateGetInfo actionStateGI;
		private final Vector2fState state = new Vector2fState();
		private final Vector2f vectorState = new Vector2f();

		private Vector2fAction(XrAction xrAction) {
			super(xrAction);
			actionStateGI = XrActionStateGetInfo.calloc().type$Default().action(xrAction);
		}

		@Override
		void _OpenXRAction_() {
			super._OpenXRAction_();
			xrState.free();
			actionStateGI.free();
		}

		@Override
		void poll(OpenXRSession session) {
			session.pollVector2fAction(actionStateGI, xrState);
			vectorState.set(xrState.currentState().x(), xrState.currentState().y());
		}

		private Vector2fState getState() {
			return state;
		}

		public final class Vector2fState {
			public Vector2fc currentState() {
				return vectorState;
			}

			public boolean changedSinceLastSync() {
				return xrState.changedSinceLastSync();
			}

			public long lastChangeTime() {
				return xrState.lastChangeTime();
			}

			public boolean isActive() {
				return xrState.isActive();
			}
		}
	}

	public final class PoseAction extends OpenXRAction {
		private final XrSpace xrActionSpace;
		private final XrActionStatePose xrState;
		private final XrActionStateGetInfo actionStateGI;
		private final XrSpaceLocation xrSpaceLocation;
		private final PoseState state;
		private final Vector3f positionState;
		private final Quaternionf orientationState;

		private PoseAction(XrAction xrAction, XrSpace xrActionSpace) {
			super(xrAction);
			this.xrActionSpace = xrActionSpace;
			this.xrState = XrActionStatePose.calloc().type$Default();
			this.actionStateGI = XrActionStateGetInfo.calloc().type$Default().action(xrAction);
			this.xrSpaceLocation = XrSpaceLocation.calloc().type$Default();
			this.state = new PoseState();
			this.positionState = new Vector3f();
			this.orientationState = new Quaternionf();
		}

		@Override
		void _OpenXRAction_() {
			super._OpenXRAction_();
			xrState.free();
			actionStateGI.free();
			xrSpaceLocation.free();
		}

		@Override
		void poll(OpenXRSession session) {
			session.pollPoseAction(actionStateGI, xrState);
		}

		void poll(OpenXRSession session, long time) {
			session.pollPoseAction(xrActionSpace, time, xrSpaceLocation);
			final XrPosef pose = xrSpaceLocation.pose();
			final XrVector3f position = pose.position$();
			positionState.set(position.x(), position.y(), position.z());
			final XrQuaternionf orientation = pose.orientation();
			orientationState.set(orientation.x(), orientation.y(), orientation.z(), orientation.w());
		}

		private PoseState getState() {
			return state;
		}

		public final class PoseState {
			public boolean isActive() {
				return xrState.isActive();
			}

			public boolean isOrientationTracked() {
				return (xrSpaceLocation.locationFlags() & XR10.XR_SPACE_LOCATION_ORIENTATION_TRACKED_BIT) > 0;
			}

			public boolean isOrientationValid() {
				return (xrSpaceLocation.locationFlags() & XR10.XR_SPACE_LOCATION_ORIENTATION_VALID_BIT) > 0;
			}

			public boolean isPositionTracked() {
				return (xrSpaceLocation.locationFlags() & XR10.XR_SPACE_LOCATION_POSITION_TRACKED_BIT) > 0;
			}

			public boolean isPositionValid() {
				return (xrSpaceLocation.locationFlags() & XR10.XR_SPACE_LOCATION_POSITION_VALID_BIT) > 0;
			}

			public Vector3fc getPosition() {
				return positionState;
			}

			public Quaternionfc getOrientation() {
				return orientationState;
			}
		}
	}

	private record ActionBinding(XrAction xrAction, String binding) {
	}
}
