package openxr;

import io.vavr.collection.Seq;

import org.lwjgl.openxr.XR11;

public class OpenXR1_1 {

	private OpenXR1_1() {
	}

	public static OpenXRInstance createInstance(String appName, Seq<String> extensions, Seq<String> layers) {
		return OpenXR1_0.createInstance(XR11.XR_API_VERSION_1_1, appName, extensions, layers);
	}

	public static final Seq<String> enumInstanceExtensions() {
		return OpenXR1_0.enumInstanceExtensions();
	}

	public static final Seq<String> enumApiLayers() {
		return OpenXR1_0.enumApiLayers();
	}
}
