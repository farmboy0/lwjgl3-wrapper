package openxr;

import java.util.Optional;

import io.vavr.API;
import io.vavr.collection.Seq;
import io.vavr.collection.Set;

public enum OpenXRAPILayer {
	XR_APILAYER_LUNARG_API_DUMP("XR_APILAYER_LUNARG_api_dump"), //
	XR_APILAYER_LUNARG_CORE_VALIDATION("XR_APILAYER_LUNARG_core_validation"), //
	;

	public static final Seq<String> API_LAYERS = OpenXR1_0.enumApiLayers();

	private final String name;

	private OpenXRAPILayer(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public boolean isAvailable() {
		return API_LAYERS.contains(name);
	}

	public static boolean isAvailable(String name) {
		return API_LAYERS.contains(name);
	}

	public void ifAvailable(Runnable available) {
		if (isAvailable())
			available.run();
	}

	public static void ifAvailable(String name, Runnable available) {
		if (isAvailable(name))
			available.run();
	}

	public void ifAvailable(Runnable available, Runnable notAvailable) {
		if (isAvailable())
			available.run();
		else
			notAvailable.run();
	}

	public static void ifAvailable(String name, Runnable available, Runnable notAvailable) {
		if (isAvailable(name))
			available.run();
		else
			notAvailable.run();
	}

	private static OpenXRException unavailable(String name) {
		return new OpenXRException(String.format("API layer %s is not available.", name));
	}

	public Optional<OpenXRAPILayer> toOptional() {
		return isAvailable() ? Optional.of(this) : Optional.empty();
	}

	public static Optional<String> toOptional(String name) {
		return isAvailable(name) ? Optional.of(name) : Optional.empty();
	}

	public static OpenXRApiLayerBuilder builder() {
		return new OpenXRApiLayerBuilder();
	}

	public static class OpenXRApiLayerBuilder {
		private Set<String> apiLayers = API.Set();

		private OpenXRApiLayerBuilder() {
		}

		public OpenXRApiLayerBuilder require(OpenXRAPILayer apiLayer) throws OpenXRException {
			if (!apiLayer.isAvailable())
				throw OpenXRAPILayer.unavailable(apiLayer.getName());
			apiLayers = apiLayers.add(apiLayer.getName());
			return this;
		}

		public OpenXRApiLayerBuilder require(String apiLayer) throws OpenXRException {
			if (!OpenXRAPILayer.isAvailable(apiLayer))
				throw OpenXRAPILayer.unavailable(apiLayer);
			apiLayers = apiLayers.add(apiLayer);
			return this;
		}

		public OpenXRApiLayerBuilder useIfAvailable(OpenXRAPILayer apiLayer) {
			if (apiLayer.isAvailable())
				apiLayers = apiLayers.add(apiLayer.getName());
			return this;
		}

		public OpenXRApiLayerBuilder useIfAvailable(String apiLayer) {
			if (OpenXRAPILayer.isAvailable(apiLayer))
				apiLayers = apiLayers.add(apiLayer);
			return this;
		}

		public OpenXRApiLayerBuilder preferOne(OpenXRAPILayer apiLayer, OpenXRAPILayer... others)
			throws OpenXRException {

			OpenXRAPILayer found = API.Seq(others)
				.prepend(apiLayer)
				.find(OpenXRAPILayer::isAvailable)
				.getOrElseThrow(() -> new OpenXRException(
					preferMsg(apiLayer.getName(), API.Seq(others).map(OpenXRAPILayer::getName))));
			apiLayers = apiLayers.add(found.getName());
			return this;
		}

		public OpenXRApiLayerBuilder preferOne(String apiLayer, String... others) throws OpenXRException {
			String found = API.Seq(others)
				.prepend(apiLayer)
				.find(OpenXRAPILayer::isAvailable)
				.getOrElseThrow(() -> new OpenXRException(preferMsg(apiLayer, API.Seq(others))));
			apiLayers = apiLayers.add(found);
			return this;
		}

		private String preferMsg(String apiLayer, Seq<String> others) {
			return others.prepend(apiLayer).mkString("None of the api layers", ", ", "is available.");
		}

		public Seq<String> build() {
			return apiLayers.toArray();
		}
	}
}
