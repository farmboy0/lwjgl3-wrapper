package openxr;

public interface TwoDimensionalBindingsBuilder extends OpenXRBindingsBuilder {

	TwoDimensionalBindingsBuilder onDaydreamUseLeftTrackpadXYValues();

	TwoDimensionalBindingsBuilder onDaydreamUseRightTrackpadXYValues();

	TwoDimensionalBindingsBuilder onViveUseLeftTrackpadXYValues();

	TwoDimensionalBindingsBuilder onViveUseRightTrackpadXYValues();

	TwoDimensionalBindingsBuilder onWMRUseLeftThumbstickXYValues();

	TwoDimensionalBindingsBuilder onWMRUseLeftTrackpadXYValues();

	TwoDimensionalBindingsBuilder onWMRUseRightThumbstickXYValues();

	TwoDimensionalBindingsBuilder onWMRUseRightTrackpadXYValues();

	TwoDimensionalBindingsBuilder onOculusGoUseLeftTrackpadXYValues();

	TwoDimensionalBindingsBuilder onOculusGoUseRightTrackpadXYValues();

	TwoDimensionalBindingsBuilder onOculusTouchUseLeftThumbstickXYValues();

	TwoDimensionalBindingsBuilder onOculusTouchUseRightThumbstickXYValues();

	TwoDimensionalBindingsBuilder onIndexUseLeftThumbstickXYValues();

	TwoDimensionalBindingsBuilder onIndexUseLeftTrackpadXYValues();

	TwoDimensionalBindingsBuilder onIndexUseRightThumbstickXYValues();

	TwoDimensionalBindingsBuilder onIndexUseRightTrackpadXYValues();
}
