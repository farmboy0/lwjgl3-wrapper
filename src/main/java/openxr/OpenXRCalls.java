package openxr;

import static org.lwjgl.system.MemoryUtil.memAllocInt;
import static org.lwjgl.system.MemoryUtil.memAllocPointer;
import static org.lwjgl.system.MemoryUtil.memFree;
import static org.lwjgl.system.MemoryUtil.memPutInt;
import static org.lwjgl.system.MemoryUtil.nmemFree;

import java.awt.Dimension;
import java.nio.Buffer;
import java.nio.IntBuffer;
import java.util.function.IntFunction;
import java.util.function.ToIntBiFunction;
import java.util.function.ToIntFunction;

import io.vavr.Tuple2;
import io.vavr.collection.Seq;
import io.vavr.control.Try;

import org.lwjgl.PointerBuffer;
import org.lwjgl.openxr.XR10;
import org.lwjgl.openxr.XrExtent2Di;
import org.lwjgl.system.CustomBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.MemoryUtil;
import org.lwjgl.system.Struct;
import org.lwjgl.system.StructBuffer;

interface OpenXRCalls {
	public static PointerBuffer toPointerBuffer(Seq<String> stringList) {
		final PointerBuffer result = memAllocPointer(stringList.size());
		stringList.map(MemoryUtil::memUTF8).forEach(result::put);
		result.flip();
		return result;
	}

	public static PointerBuffer toPointerBuffer(Seq<String> stringList, MemoryStack stack) {
		final PointerBuffer result = stack.mallocPointer(stringList.size());
		stringList.map(stack::UTF8).forEach(result::put);
		result.flip();
		return result;
	}

	public static Dimension toDimension(XrExtent2Di ext) {
		return new Dimension(ext.width(), ext.height());
	}

	public static void freePointerBuffer(PointerBuffer buffer) {
		if (buffer == null)
			return;
		for (int i = 0; i < buffer.limit(); i++) {
			nmemFree(buffer.get(i));
		}
		buffer.free();
	}

	public static <B extends Buffer> Try<B> toTry(B buffer, ToIntFunction<B> openXrCall, String msg) {
		int ret = openXrCall.applyAsInt(buffer);
		if (!XR10.XR_SUCCEEDED(ret)) {
			memFree(buffer);
			return Try.failure(new OpenXRError(ret, msg));
		}
		return Try.success(buffer);
	}

	public static <B extends Buffer> Try<B> toTryStack(B bufferOnStack, ToIntFunction<B> openXrCall, String msg) {
		int ret = openXrCall.applyAsInt(bufferOnStack);
		if (!XR10.XR_SUCCEEDED(ret)) {
			return Try.failure(new OpenXRError(ret, msg));
		}
		return Try.success(bufferOnStack);
	}

	public static <B extends Buffer> Try<Tuple2<Integer, B>> toTryBuffer(IntFunction<B> bufferCreator,
		ToIntBiFunction<IntBuffer, B> openXrCall, String msg) {

		final ToIntFunction<IntBuffer> fCount = bCount -> openXrCall.applyAsInt(bCount, null);
		return toTry(memAllocInt(1), fCount, msg).mapTry(bCount -> {
			final int count = bCount.get(0);
			final ToIntFunction<B> fData = bData -> openXrCall.applyAsInt(bCount, bData);
			return toTry(bufferCreator.apply(count), fData, msg) //
				.mapTry(bData -> new Tuple2<>(count, bData)) //
				.andFinally(() -> memFree(bCount));
		}).get();
	}

	public static <B extends CustomBuffer<B>> Try<B> toTry(B buffer, ToIntFunction<B> openXrCall, String msg) {
		int ret = openXrCall.applyAsInt(buffer);
		if (!XR10.XR_SUCCEEDED(ret)) {
			memFree(buffer);
			return Try.failure(new OpenXRError(ret, msg));
		}
		return Try.success(buffer);
	}

	public static <B extends CustomBuffer<B>> Try<B> toTryStack(B bufferOnStack, ToIntFunction<B> openXrCall,
		String msg) {

		int ret = openXrCall.applyAsInt(bufferOnStack);
		if (!XR10.XR_SUCCEEDED(ret)) {
			return Try.failure(new OpenXRError(ret, msg));
		}
		return Try.success(bufferOnStack);
	}

	public static <B extends CustomBuffer<B>> Try<Tuple2<Integer, B>> toTryCustom(IntFunction<B> bufferCreator,
		ToIntBiFunction<IntBuffer, B> openXrCall, String msg) {

		final ToIntFunction<IntBuffer> fCount = bCount -> openXrCall.applyAsInt(bCount, null);
		return toTry(memAllocInt(1), fCount, msg).mapTry(bCount -> {
			final int count = bCount.get(0);
			final ToIntFunction<B> fData = bData -> openXrCall.applyAsInt(bCount, bData);
			return toTry(bufferCreator.apply(count), fData, msg) //
				.mapTry(bData -> new Tuple2<>(count, bData)) //
				.andFinally(() -> memFree(bCount));
		}).get();
	}

	public static <S extends Struct<S>> Try<S> toTry(S struct, ToIntFunction<S> openXrCall, String msg) {
		int ret = openXrCall.applyAsInt(struct);
		if (!XR10.XR_SUCCEEDED(ret)) {
			struct.free();
			return Try.failure(new OpenXRError(ret, msg));
		}
		return Try.success(struct);
	}

	public static <S extends Struct<S>> Try<S> toTryStack(S structOnStack, ToIntFunction<S> openXrCall, String msg) {
		int ret = openXrCall.applyAsInt(structOnStack);
		if (!XR10.XR_SUCCEEDED(ret)) {
			return Try.failure(new OpenXRError(ret, msg));
		}
		return Try.success(structOnStack);
	}

	public static <S extends Struct<S>, T extends StructBuffer<S, T>> T fill(T buffer, int offset, int value) {
		long ptr = buffer.address() + offset;
		int stride = buffer.sizeof();
		for (int i = 0; i < buffer.limit(); i++) {
			memPutInt(ptr + i * stride, value);
		}
		return buffer;
	}
}
