package openxr;

import org.joml.Quaternionfc;
import org.joml.Vector3fc;

public interface OpenXREventListener {

	void eventsLost(int lostEventCount);

	void instanceLoss(long lossTime);

	void sessionStateChange(long session, OpenXRSessionState state, long time);

	void interactionProfileChange(long session);

	void refSpaceChange(long session, int referenceSpaceType, long changeTime, boolean poseValid,
		Vector3fc positionInPreviousSpace, Quaternionfc orientationInPreviousSpace);

}
