package openxr;

import io.vavr.API;

public enum OpenXRSessionState {
	UNKNOWN(0), //
	IDLE(1), //
	READY(2), //
	SYNCHRONIZED(3), //
	VISIBLE(4), //
	FOCUSED(5), //
	STOPPING(6), //
	LOSS_PENDING(7), //
	EXITING(8), //
	;

	private final int xrVal;

	private OpenXRSessionState(int xrVal) {
		this.xrVal = xrVal;
	}

	static final OpenXRSessionState from(int value) {
		return API.Seq(values()).find(state -> state.xrVal == value).getOrNull();
	}
}
