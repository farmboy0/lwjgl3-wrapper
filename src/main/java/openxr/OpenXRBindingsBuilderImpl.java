package openxr;

import static io.vavr.API.Seq;

import javax.annotation.Nonnull;

import io.vavr.collection.Seq;

import openxr.OpenXRActionSet.OpenXRAction;

final class OpenXRBindingsBuilderImpl
	implements OneDimensionalBindingsBuilder, TwoDimensionalBindingsBuilder, PoseBindingsBuilder {
	private static final String SIMPLE = "/interaction_profiles/khr/simple_controller";
	private static final String DAYDREAM = "/interaction_profiles/google/daydream_controller";
	private static final String VIVE = "/interaction_profiles/htc/vive_controller";
	private static final String VIVE_PRO = "/interaction_profiles/htc/vive_pro";
	private static final String WMR = "/interaction_profiles/microsoft/motion_controller";
	private static final String OCULUS_GO = "/interaction_profiles/oculus/go_controller";
	private static final String OCULUS_TOUCH = "/interaction_profiles/oculus/touch_controller";
	private static final String INDEX = "/interaction_profiles/valve/index_controller";

	private static final Seq<String> POSE_PROFILES = Seq(SIMPLE, DAYDREAM, VIVE, WMR, OCULUS_GO, OCULUS_TOUCH, INDEX);

	private static final String HEAD = "/user/head";
	private static final String LEFT_HAND = "/user/hand/left";
	private static final String RIGHT_HAND = "/user/hand/right";

	private static final String SYSTEM_CLICK = "/input/system/click";
	private static final String SELECT_CLICK = "/input/select/click";
	private static final String MENU_CLICK = "/input/menu/click";
	private static final String A_CLICK = "/input/a/click";
	private static final String A_TOUCH = "/input/a/touch";
	private static final String B_CLICK = "/input/b/click";
	private static final String B_TOUCH = "/input/b/touch";
	private static final String TRIGGER_VALUE = "/input/trigger/value";
	private static final String TRIGGER_CLICK = "/input/trigger/click";
	private static final String TRIGGER_TOUCH = "/input/trigger/touch";
	private static final String SQUEEZE_VALUE = "/input/squeeze/value";
	private static final String SQUEEZE_CLICK = "/input/squeeze/click";
	private static final String THUMBSTICK_X = "/input/thumbstick/x";
	private static final String THUMBSTICK_Y = "/input/thumbstick/y";
	private static final String THUMBSTICK_CLICK = "/input/thumbstick/click";
	private static final String THUMBSTICK_TOUCH = "/input/thumbstick/touch";
	private static final String TRACKPAD_X = "/input/trackpad/x";
	private static final String TRACKPAD_Y = "/input/trackpad/y";
	private static final String TRACKPAD_CLICK = "/input/trackpad/click";
	private static final String TRACKPAD_TOUCH = "/input/trackpad/touch";

	private static final String THUMBSTICK = "/input/thumbstick";
	private static final String TRACKPAD = "/input/trackpad";

	private static final String AIM_POSE = "/input/aim/pose";
	private static final String GRIP_POSE = "/input/grip/pose";

	private final OpenXRActionSet actionSet;
	private final OpenXRAction action;

	OpenXRBindingsBuilderImpl(OpenXRActionSet actionSet, OpenXRAction action) {
		this.actionSet = actionSet;
		this.action = action;
	}

	@Override
	public OneDimensionalBindingsBuilder addBooleanAction(@Nonnull String id, @Nonnull String description) {
		return actionSet.addBooleanAction(id, description);
	}

	@Override
	public OneDimensionalBindingsBuilder addFloatAction(@Nonnull String id, @Nonnull String description) {
		return actionSet.addFloatAction(id, description);
	}

	@Override
	public TwoDimensionalBindingsBuilder addVector2fAction(@Nonnull String id, @Nonnull String description) {
		return actionSet.addVector2fAction(id, description);
	}

	@Override
	public PoseBindingsBuilder addPoseAction(@Nonnull String id, @Nonnull String description,
		@Nonnull OpenXRSession session) {

		return actionSet.addPoseAction(id, description, session);
	}

	@Override
	public OpenXRActionSet suggestBindings() {
		actionSet.suggestBindings();
		return actionSet;
	}

	// KHR Simple

	@Override
	public OneDimensionalBindingsBuilder onSimpleUseLeftSelectClick() {
		action.addBinding(SIMPLE, LEFT_HAND + SELECT_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onSimpleUseRightSelectClick() {
		action.addBinding(SIMPLE, RIGHT_HAND + SELECT_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onSimpleUseLeftMenuClick() {
		action.addBinding(SIMPLE, LEFT_HAND + MENU_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onSimpleUseRightMenuClick() {
		action.addBinding(SIMPLE, RIGHT_HAND + MENU_CLICK);
		return this;
	}

	// Google Daydream

	@Override
	public OneDimensionalBindingsBuilder onDaydreamUseLeftSelectClick() {
		action.addBinding(DAYDREAM, LEFT_HAND + SELECT_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onDaydreamUseRightSelectClick() {
		action.addBinding(DAYDREAM, RIGHT_HAND + SELECT_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onDaydreamUseLeftTrackpadXValue() {
		action.addBinding(DAYDREAM, LEFT_HAND + TRACKPAD_X);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onDaydreamUseRightTrackpadXValue() {
		action.addBinding(DAYDREAM, RIGHT_HAND + TRACKPAD_X);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onDaydreamUseLeftTrackpadYValue() {
		action.addBinding(DAYDREAM, LEFT_HAND + TRACKPAD_Y);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onDaydreamUseRightTrackpadYValue() {
		action.addBinding(DAYDREAM, RIGHT_HAND + TRACKPAD_Y);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onDaydreamUseLeftTrackpadClick() {
		action.addBinding(DAYDREAM, LEFT_HAND + TRACKPAD_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onDaydreamUseRightTrackpadClick() {
		action.addBinding(DAYDREAM, RIGHT_HAND + TRACKPAD_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onDaydreamUseLeftTrackpadTouch() {
		action.addBinding(DAYDREAM, LEFT_HAND + TRACKPAD_TOUCH);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onDaydreamUseRightTrackpadTouch() {
		action.addBinding(DAYDREAM, RIGHT_HAND + TRACKPAD_TOUCH);
		return this;
	}

	// HTC Vive

	@Override
	public OneDimensionalBindingsBuilder onViveUseLeftSystemClick() {
		action.addBinding(VIVE, LEFT_HAND + SYSTEM_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onViveUseRightSystemClick() {
		action.addBinding(VIVE, RIGHT_HAND + SYSTEM_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onViveUseLeftSqueezeClick() {
		action.addBinding(VIVE, LEFT_HAND + SQUEEZE_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onViveUseRightSqueezeClick() {
		action.addBinding(VIVE, RIGHT_HAND + SQUEEZE_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onViveUseLeftMenuClick() {
		action.addBinding(VIVE, LEFT_HAND + MENU_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onViveUseRightMenuClick() {
		action.addBinding(VIVE, RIGHT_HAND + MENU_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onViveUseLeftTriggerValue() {
		action.addBinding(VIVE, LEFT_HAND + TRIGGER_VALUE);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onViveUseRightTriggerValue() {
		action.addBinding(VIVE, RIGHT_HAND + TRIGGER_VALUE);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onViveUseLeftTriggerClick() {
		action.addBinding(VIVE, LEFT_HAND + TRIGGER_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onViveUseRightTriggerClick() {
		action.addBinding(VIVE, RIGHT_HAND + TRIGGER_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onViveUseLeftTrackpadXValue() {
		action.addBinding(VIVE, LEFT_HAND + TRACKPAD_X);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onViveUseRightTrackpadXValue() {
		action.addBinding(VIVE, RIGHT_HAND + TRACKPAD_X);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onViveUseLeftTrackpadYValue() {
		action.addBinding(VIVE, LEFT_HAND + TRACKPAD_Y);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onViveUseRightTrackpadYValue() {
		action.addBinding(VIVE, RIGHT_HAND + TRACKPAD_Y);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onViveUseLeftTrackpadClick() {
		action.addBinding(VIVE, LEFT_HAND + TRACKPAD_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onViveUseRightTrackpadClick() {
		action.addBinding(VIVE, RIGHT_HAND + TRACKPAD_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onViveUseLeftTrackpadTouch() {
		action.addBinding(VIVE, LEFT_HAND + TRACKPAD_TOUCH);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onViveUseRightTrackpadTouch() {
		action.addBinding(VIVE, RIGHT_HAND + TRACKPAD_TOUCH);
		return this;
	}

	// HTC Vive Pro

	@Override
	public OneDimensionalBindingsBuilder onViveProUseHeadSystemClick() {
		action.addBinding(VIVE_PRO, HEAD + SYSTEM_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onViveProUseHeadVolumeUpClick() {
		action.addBinding(VIVE_PRO, HEAD + "/input/volume_up/click");
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onViveProUseHeadVolumeDownClick() {
		action.addBinding(VIVE_PRO, HEAD + "/input/volume_down/click");
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onViveProUseHeadMuteMicClick() {
		action.addBinding(VIVE_PRO, HEAD + "/input/mute_mic/click");
		return this;
	}

	// Microsoft WMR

	@Override
	public OneDimensionalBindingsBuilder onWMRUseLeftMenuClick() {
		action.addBinding(WMR, LEFT_HAND + MENU_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onWMRUseRightMenuClick() {
		action.addBinding(WMR, RIGHT_HAND + MENU_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onWMRUseLeftSqueezeClick() {
		action.addBinding(WMR, LEFT_HAND + SQUEEZE_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onWMRxUseRightSqueezeClick() {
		action.addBinding(WMR, RIGHT_HAND + SQUEEZE_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onWMRUseLeftTriggerValue() {
		action.addBinding(WMR, LEFT_HAND + TRIGGER_VALUE);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onWMRUseRightTriggerValue() {
		action.addBinding(WMR, RIGHT_HAND + TRIGGER_VALUE);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onWMRUseLeftThumbstickXValue() {
		action.addBinding(WMR, LEFT_HAND + THUMBSTICK_X);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onWMRUseRightThumbstickXValue() {
		action.addBinding(WMR, RIGHT_HAND + THUMBSTICK_X);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onWMRUseLeftThumbstickYValue() {
		action.addBinding(WMR, LEFT_HAND + THUMBSTICK_Y);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onWMRUseRightThumbstickYValue() {
		action.addBinding(WMR, RIGHT_HAND + THUMBSTICK_Y);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onWMRUseLeftThumbstickClick() {
		action.addBinding(WMR, LEFT_HAND + THUMBSTICK_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onWMRUseRightThumbstickClick() {
		action.addBinding(WMR, RIGHT_HAND + THUMBSTICK_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onWMRUseLeftTrackpadXValue() {
		action.addBinding(WMR, LEFT_HAND + TRACKPAD_X);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onWMRUseRightTrackpadXValue() {
		action.addBinding(WMR, RIGHT_HAND + TRACKPAD_X);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onWMRUseLeftTrackpadYValue() {
		action.addBinding(WMR, LEFT_HAND + TRACKPAD_Y);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onWMRUseRightTrackpadYValue() {
		action.addBinding(WMR, RIGHT_HAND + TRACKPAD_Y);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onWMRUseLeftTrackpadClick() {
		action.addBinding(WMR, LEFT_HAND + TRACKPAD_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onWMRUseRightTrackpadClick() {
		action.addBinding(WMR, RIGHT_HAND + TRACKPAD_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onWMRUseLeftTrackpadTouch() {
		action.addBinding(WMR, LEFT_HAND + TRACKPAD_TOUCH);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onWMRUseRightTrackpadTouch() {
		action.addBinding(WMR, RIGHT_HAND + TRACKPAD_TOUCH);
		return this;
	}

	// Oculus GO

	@Override
	public OneDimensionalBindingsBuilder onOculusGoUseLeftSystemClick() {
		action.addBinding(OCULUS_GO, LEFT_HAND + SYSTEM_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusGoUseRightSystemClick() {
		action.addBinding(OCULUS_GO, RIGHT_HAND + SYSTEM_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusGoUseLeftBackClick() {
		action.addBinding(OCULUS_GO, LEFT_HAND + "/input/back/click");
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusGoUseRightBackClick() {
		action.addBinding(OCULUS_GO, RIGHT_HAND + "/input/back/click");
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusGoUseLeftTriggerClick() {
		action.addBinding(OCULUS_GO, LEFT_HAND + TRIGGER_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusGoUseRightTriggerClick() {
		action.addBinding(OCULUS_GO, RIGHT_HAND + TRIGGER_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusGoUseLeftTrackpadXValue() {
		action.addBinding(OCULUS_GO, LEFT_HAND + TRACKPAD_X);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusGoUseRightTrackpadXValue() {
		action.addBinding(OCULUS_GO, RIGHT_HAND + TRACKPAD_X);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusGoUseLeftTrackpadYValue() {
		action.addBinding(OCULUS_GO, LEFT_HAND + TRACKPAD_Y);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusGoUseRightTrackpadYValue() {
		action.addBinding(OCULUS_GO, RIGHT_HAND + TRACKPAD_Y);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusGoUseLeftTrackpadClick() {
		action.addBinding(OCULUS_GO, LEFT_HAND + TRACKPAD_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusGoUseRightTrackpadClick() {
		action.addBinding(OCULUS_GO, RIGHT_HAND + TRACKPAD_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusGoUseLeftTrackpadTouch() {
		action.addBinding(OCULUS_GO, LEFT_HAND + TRACKPAD_TOUCH);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusGoUseRightTrackpadTouch() {
		action.addBinding(OCULUS_GO, RIGHT_HAND + TRACKPAD_TOUCH);
		return this;
	}

	// Oculus Touch

	@Override
	public OneDimensionalBindingsBuilder onOculusTouchUseLeftMenuClick() {
		action.addBinding(OCULUS_TOUCH, LEFT_HAND + MENU_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusTouchUseRightSystemClick() {
		action.addBinding(OCULUS_TOUCH, RIGHT_HAND + SYSTEM_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusTouchUseLeftXClick() {
		action.addBinding(OCULUS_TOUCH, LEFT_HAND + "/input/x/click");
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusTouchUseRightAClick() {
		action.addBinding(OCULUS_TOUCH, RIGHT_HAND + A_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusTouchUseLeftXTouch() {
		action.addBinding(OCULUS_TOUCH, LEFT_HAND + "/input/x/touch");
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusTouchUseRightATouch() {
		action.addBinding(OCULUS_TOUCH, RIGHT_HAND + A_TOUCH);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusTouchUseLeftYClick() {
		action.addBinding(OCULUS_TOUCH, LEFT_HAND + "/input/y/click");
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusTouchUseRightBClick() {
		action.addBinding(OCULUS_TOUCH, RIGHT_HAND + B_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusTouchUseLeftYTouch() {
		action.addBinding(OCULUS_TOUCH, LEFT_HAND + "/input/y/touch");
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusTouchUseRightBTouch() {
		action.addBinding(OCULUS_TOUCH, RIGHT_HAND + B_TOUCH);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusTouchUseLeftSqueezeValue() {
		action.addBinding(OCULUS_TOUCH, LEFT_HAND + SQUEEZE_VALUE);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusTouchUseRightSqueezeValue() {
		action.addBinding(OCULUS_TOUCH, RIGHT_HAND + SQUEEZE_VALUE);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusTouchUseLeftTriggerValue() {
		action.addBinding(OCULUS_TOUCH, LEFT_HAND + TRIGGER_VALUE);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusTouchUseRightTriggerValue() {
		action.addBinding(OCULUS_TOUCH, RIGHT_HAND + TRIGGER_VALUE);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusTouchUseLeftTriggerTouch() {
		action.addBinding(OCULUS_TOUCH, LEFT_HAND + TRIGGER_TOUCH);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusTouchUseRightTriggerTouch() {
		action.addBinding(OCULUS_TOUCH, RIGHT_HAND + TRIGGER_TOUCH);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusTouchUseLeftThumbstickXValue() {
		action.addBinding(OCULUS_TOUCH, LEFT_HAND + THUMBSTICK_X);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusTouchUseRightThumbstickXValue() {
		action.addBinding(OCULUS_TOUCH, RIGHT_HAND + THUMBSTICK_X);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusTouchUseLeftThumbstickYValue() {
		action.addBinding(OCULUS_TOUCH, LEFT_HAND + THUMBSTICK_Y);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusTouchUseRightThumbstickYValue() {
		action.addBinding(OCULUS_TOUCH, RIGHT_HAND + THUMBSTICK_Y);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusTouchUseLeftThumbstickClick() {
		action.addBinding(OCULUS_TOUCH, LEFT_HAND + THUMBSTICK_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusTouchUseRightThumbstickClick() {
		action.addBinding(OCULUS_TOUCH, RIGHT_HAND + THUMBSTICK_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusTouchUseLeftThumbstickTouch() {
		action.addBinding(OCULUS_TOUCH, LEFT_HAND + THUMBSTICK_TOUCH);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusTouchUseRightThumbstickTouch() {
		action.addBinding(OCULUS_TOUCH, RIGHT_HAND + THUMBSTICK_TOUCH);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusTouchUseLeftThumprestTouch() {
		action.addBinding(OCULUS_TOUCH, LEFT_HAND + "/input/thumbrest/touch");
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onOculusTouchUseRightThumprestTouch() {
		action.addBinding(OCULUS_TOUCH, RIGHT_HAND + "/input/thumbrest/touch");
		return this;
	}

	// Valve Index

	@Override
	public OneDimensionalBindingsBuilder onIndexUseLeftSystemClick() {
		action.addBinding(INDEX, LEFT_HAND + SYSTEM_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onIndexUseRightSystemClick() {
		action.addBinding(INDEX, RIGHT_HAND + SYSTEM_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onIndexUseLeftSystemTouch() {
		action.addBinding(INDEX, LEFT_HAND + "/input/system/touch");
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onIndexUseRightSystemTouch() {
		action.addBinding(INDEX, RIGHT_HAND + "/input/system/touch");
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onIndexUseLeftAClick() {
		action.addBinding(INDEX, LEFT_HAND + A_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onIndexUseRightAClick() {
		action.addBinding(INDEX, RIGHT_HAND + A_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onIndexUseLeftATouch() {
		action.addBinding(INDEX, LEFT_HAND + A_TOUCH);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onIndexUseRightATouch() {
		action.addBinding(INDEX, RIGHT_HAND + A_TOUCH);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onIndexUseLeftBClick() {
		action.addBinding(INDEX, LEFT_HAND + B_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onIndexUseRightBClick() {
		action.addBinding(INDEX, RIGHT_HAND + B_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onIndexUseLeftBTouch() {
		action.addBinding(INDEX, LEFT_HAND + B_TOUCH);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onIndexUseRightBTouch() {
		action.addBinding(INDEX, RIGHT_HAND + B_TOUCH);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onIndexUseLeftSqueezeValue() {
		action.addBinding(INDEX, LEFT_HAND + SQUEEZE_VALUE);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onIndexUseRightSqueezeValue() {
		action.addBinding(INDEX, RIGHT_HAND + SQUEEZE_VALUE);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onIndexUseLeftSqueezeForce() {
		action.addBinding(INDEX, LEFT_HAND + "/input/squeeze/force");
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onIndexUseRightSqueezeForce() {
		action.addBinding(INDEX, RIGHT_HAND + "/input/squeeze/force");
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onIndexUseLeftTriggerValue() {
		action.addBinding(INDEX, LEFT_HAND + TRIGGER_VALUE);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onIndexUseRightTriggerValue() {
		action.addBinding(INDEX, RIGHT_HAND + TRIGGER_VALUE);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onIndexUseLeftTriggerClick() {
		action.addBinding(INDEX, LEFT_HAND + TRIGGER_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onIndexUseRightTriggerClick() {
		action.addBinding(INDEX, RIGHT_HAND + TRIGGER_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onIndexUseLeftTriggerTouch() {
		action.addBinding(INDEX, LEFT_HAND + TRIGGER_TOUCH);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onIndexUseRightTriggerTouch() {
		action.addBinding(INDEX, RIGHT_HAND + TRIGGER_TOUCH);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onIndexUseLeftThumbstickXValue() {
		action.addBinding(INDEX, LEFT_HAND + THUMBSTICK_X);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onIndexUseRightThumbstickXValue() {
		action.addBinding(INDEX, RIGHT_HAND + THUMBSTICK_X);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onIndexUseLeftThumbstickYValue() {
		action.addBinding(INDEX, LEFT_HAND + THUMBSTICK_Y);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onIndexUseRightThumbstickYValue() {
		action.addBinding(INDEX, RIGHT_HAND + THUMBSTICK_Y);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onIndexUseLeftThumbstickClick() {
		action.addBinding(INDEX, LEFT_HAND + THUMBSTICK_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onIndexUseRightThumbstickClick() {
		action.addBinding(INDEX, RIGHT_HAND + THUMBSTICK_CLICK);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onIndexUseLeftThumbstickTouch() {
		action.addBinding(INDEX, LEFT_HAND + THUMBSTICK_TOUCH);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onIndexUseRightThumbstickTouch() {
		action.addBinding(INDEX, RIGHT_HAND + THUMBSTICK_TOUCH);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onIndexUseLeftTrackpadXValue() {
		action.addBinding(INDEX, LEFT_HAND + TRACKPAD_X);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onIndexUseRightTrackpadXValue() {
		action.addBinding(INDEX, RIGHT_HAND + TRACKPAD_X);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onIndexUseLeftTrackpadYValue() {
		action.addBinding(INDEX, LEFT_HAND + TRACKPAD_Y);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onIndexUseRightTrackpadYValue() {
		action.addBinding(INDEX, RIGHT_HAND + TRACKPAD_Y);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onIndexUseLeftTrackpadForce() {
		action.addBinding(INDEX, LEFT_HAND + "/input/trackpad/force");
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onIndexUseRightTrackpadForce() {
		action.addBinding(INDEX, RIGHT_HAND + "/input/trackpad/force");
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onIndexUseLeftTrackpadTouch() {
		action.addBinding(INDEX, LEFT_HAND + TRACKPAD_TOUCH);
		return this;
	}

	@Override
	public OneDimensionalBindingsBuilder onIndexUseRightTrackpadTouch() {
		action.addBinding(INDEX, RIGHT_HAND + TRACKPAD_TOUCH);
		return this;
	}

	// Vector2f Bindings

	@Override
	public TwoDimensionalBindingsBuilder onDaydreamUseLeftTrackpadXYValues() {
		action.addBinding(DAYDREAM, LEFT_HAND + TRACKPAD);
		return this;
	}

	@Override
	public TwoDimensionalBindingsBuilder onDaydreamUseRightTrackpadXYValues() {
		action.addBinding(DAYDREAM, RIGHT_HAND + TRACKPAD);
		return this;
	}

	@Override
	public TwoDimensionalBindingsBuilder onViveUseLeftTrackpadXYValues() {
		action.addBinding(VIVE, LEFT_HAND + TRACKPAD);
		return this;
	}

	@Override
	public TwoDimensionalBindingsBuilder onViveUseRightTrackpadXYValues() {
		action.addBinding(VIVE, RIGHT_HAND + TRACKPAD);
		return this;
	}

	@Override
	public TwoDimensionalBindingsBuilder onWMRUseLeftThumbstickXYValues() {
		action.addBinding(WMR, LEFT_HAND + THUMBSTICK);
		return this;
	}

	@Override
	public TwoDimensionalBindingsBuilder onWMRUseRightThumbstickXYValues() {
		action.addBinding(WMR, RIGHT_HAND + THUMBSTICK);
		return this;
	}

	@Override
	public TwoDimensionalBindingsBuilder onWMRUseLeftTrackpadXYValues() {
		action.addBinding(WMR, LEFT_HAND + TRACKPAD);
		return this;
	}

	@Override
	public TwoDimensionalBindingsBuilder onWMRUseRightTrackpadXYValues() {
		action.addBinding(WMR, RIGHT_HAND + TRACKPAD);
		return this;
	}

	@Override
	public TwoDimensionalBindingsBuilder onOculusGoUseLeftTrackpadXYValues() {
		action.addBinding(OCULUS_GO, LEFT_HAND + TRACKPAD);
		return this;
	}

	@Override
	public TwoDimensionalBindingsBuilder onOculusGoUseRightTrackpadXYValues() {
		action.addBinding(OCULUS_GO, RIGHT_HAND + TRACKPAD);
		return this;
	}

	@Override
	public TwoDimensionalBindingsBuilder onOculusTouchUseLeftThumbstickXYValues() {
		action.addBinding(OCULUS_TOUCH, LEFT_HAND + THUMBSTICK);
		return this;
	}

	@Override
	public TwoDimensionalBindingsBuilder onOculusTouchUseRightThumbstickXYValues() {
		action.addBinding(OCULUS_TOUCH, RIGHT_HAND + THUMBSTICK);
		return this;
	}

	@Override
	public TwoDimensionalBindingsBuilder onIndexUseLeftThumbstickXYValues() {
		action.addBinding(INDEX, LEFT_HAND + THUMBSTICK);
		return this;
	}

	@Override
	public TwoDimensionalBindingsBuilder onIndexUseRightThumbstickXYValues() {
		action.addBinding(INDEX, RIGHT_HAND + THUMBSTICK);
		return this;
	}

	@Override
	public TwoDimensionalBindingsBuilder onIndexUseLeftTrackpadXYValues() {
		action.addBinding(INDEX, LEFT_HAND + TRACKPAD);
		return this;
	}

	@Override
	public TwoDimensionalBindingsBuilder onIndexUseRightTrackpadXYValues() {
		action.addBinding(INDEX, RIGHT_HAND + TRACKPAD);
		return this;
	}

	@Override
	public OpenXRBindingsBuilder useLeftHandAim() {
		POSE_PROFILES.forEach(profile -> action.addBinding(profile, LEFT_HAND + AIM_POSE));
		return this;
	}

	@Override
	public OpenXRBindingsBuilder useRightHandAim() {
		POSE_PROFILES.forEach(profile -> action.addBinding(profile, RIGHT_HAND + AIM_POSE));
		return this;
	}

	@Override
	public OpenXRBindingsBuilder useLeftHandGrip() {
		POSE_PROFILES.forEach(profile -> action.addBinding(profile, LEFT_HAND + GRIP_POSE));
		return this;
	}

	@Override
	public OpenXRBindingsBuilder useRightHandGrip() {
		POSE_PROFILES.forEach(profile -> action.addBinding(profile, RIGHT_HAND + GRIP_POSE));
		return this;
	}
}