package openxr;

import static openxr.OpenXRCalls.toTryBuffer;
import static openxr.OpenXRCalls.toTryStack;

import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.util.function.ToIntBiFunction;
import java.util.function.ToIntFunction;

import io.vavr.collection.List;
import io.vavr.collection.Seq;

import org.lwjgl.PointerBuffer;
import org.lwjgl.openxr.XR10;
import org.lwjgl.openxr.XrAction;
import org.lwjgl.openxr.XrActionSpaceCreateInfo;
import org.lwjgl.openxr.XrActionStateBoolean;
import org.lwjgl.openxr.XrActionStateFloat;
import org.lwjgl.openxr.XrActionStateGetInfo;
import org.lwjgl.openxr.XrActionStatePose;
import org.lwjgl.openxr.XrActionStateVector2f;
import org.lwjgl.openxr.XrActionsSyncInfo;
import org.lwjgl.openxr.XrActiveActionSet;
import org.lwjgl.openxr.XrCompositionLayerProjection;
import org.lwjgl.openxr.XrCompositionLayerProjectionView;
import org.lwjgl.openxr.XrFrameBeginInfo;
import org.lwjgl.openxr.XrFrameEndInfo;
import org.lwjgl.openxr.XrFrameState;
import org.lwjgl.openxr.XrFrameWaitInfo;
import org.lwjgl.openxr.XrPosef;
import org.lwjgl.openxr.XrQuaternionf;
import org.lwjgl.openxr.XrReferenceSpaceCreateInfo;
import org.lwjgl.openxr.XrSession;
import org.lwjgl.openxr.XrSessionActionSetsAttachInfo;
import org.lwjgl.openxr.XrSessionBeginInfo;
import org.lwjgl.openxr.XrSpace;
import org.lwjgl.openxr.XrSpaceLocation;
import org.lwjgl.openxr.XrSwapchain;
import org.lwjgl.openxr.XrSwapchainCreateInfo;
import org.lwjgl.openxr.XrVector3f;
import org.lwjgl.openxr.XrView;
import org.lwjgl.openxr.XrViewLocateInfo;
import org.lwjgl.openxr.XrViewState;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.MemoryUtil;

import openxr.OpenXRInstance.ViewConfig;

public class OpenXRSession {
	private static final String ERR_CREATE_REFSPACE = "Failed to create reference space";
	private static final String ERR_CREATE_ACTIONSPACE = "Failed to create action space";
	private static final String ERR_ENUM_SWAPCHAIN_FORMATS = "Failed to enumerate swapchain formats";
	private static final String ERR_CREATE_SWAPCHAIN = null;

	private XrSession xrSession;

	OpenXRSession(XrSession xrSession) {
		this.xrSession = xrSession;
	}

	public void _OpenXRSession_() {
		xrSessionBegin.free();
		xrFrameState.free();
		xrFrameWaitInfo.free();
		xrFrameBeginInfo.free();
		xrFrameEndInfo.free();
		layerProjection.free();
		layers.free();
		xrViewState.free();
		if (xrViewLocateInfo != null) {
			xrViewLocateInfo.free();
		}
		MemoryUtil.memFree(xrViewCount);
		if (xrViews != null) {
			xrViews.free();
		}
		if (xrCLPViews != null) {
			xrCLPViews.free();
		}
		if (xrSpace != null) {
			XR10.xrDestroySpace(xrSpace);
			xrSpace = null;
		}
		XR10.xrDestroySession(xrSession);
		xrSession = null;
	}

	private XrSpace xrSpace;

	public void createReferenceSpace() {
		if (xrSpace != null) {
			return;
		}
		try (MemoryStack stack = MemoryStack.stackPush()) {
			final XrPosef pose = XrPosef.malloc(stack)
				.orientation(XrQuaternionf.malloc(stack).x(0).y(0).z(0).w(1))
				.position$(XrVector3f.calloc(stack));
			final XrReferenceSpaceCreateInfo refSpaceCI = XrReferenceSpaceCreateInfo.malloc(stack)
				.type$Default()
				.next(MemoryUtil.NULL)
				.referenceSpaceType(XR10.XR_REFERENCE_SPACE_TYPE_STAGE)
				.poseInReferenceSpace(pose);

			final ToIntFunction<PointerBuffer> fRefSpace = bRefSpace -> XR10.xrCreateReferenceSpace(xrSession,
				refSpaceCI, bRefSpace);
			xrSpace = toTryStack(stack.mallocPointer(1), fRefSpace, ERR_CREATE_REFSPACE).mapTry(bRefSpace -> {
				return new XrSpace(bRefSpace.get(0), xrSession);
			}).get();
		}
		initViewLocateInfo();
	}

	XrSpace createActionSpace(XrAction action) {
		try (MemoryStack stack = MemoryStack.stackPush()) {
			final XrPosef pose = XrPosef.malloc(stack)
				.orientation(XrQuaternionf.malloc(stack).x(0).y(0).z(0).w(1))
				.position$(XrVector3f.calloc(stack));
			final XrActionSpaceCreateInfo actionSpaceCI = XrActionSpaceCreateInfo.malloc(stack)
				.type$Default()
				.next(MemoryUtil.NULL)
				.action(action)
				.poseInActionSpace(pose);

			final ToIntFunction<PointerBuffer> fActionSpace = bActionSpace -> XR10.xrCreateActionSpace(xrSession,
				actionSpaceCI, bActionSpace);
			return toTryStack(stack.mallocPointer(1), fActionSpace, ERR_CREATE_ACTIONSPACE).mapTry(bActionSpace -> {
				return new XrSpace(bActionSpace.get(0), xrSession);
			}).get();
		}
	}

	public Seq<Long> enumSwapchainFormats() {
		try (MemoryStack stack = MemoryStack.stackPush()) {
			final ToIntBiFunction<IntBuffer, LongBuffer> fSCFormats = //
				(bCount, bSCFormats) -> XR10.xrEnumerateSwapchainFormats(xrSession, bCount, bSCFormats);

			return toTryBuffer(stack::mallocLong, fSCFormats, ERR_ENUM_SWAPCHAIN_FORMATS).mapTry(tSCFormats -> {
				return List.range(0, tSCFormats._1).map(tSCFormats._2::get);
			}).get();
		}
	}

	private boolean running = false;
	private boolean ended = false;

	public boolean isRunning() {
		return running;
	}

	public boolean isEnded() {
		return ended;
	}

	private XrSessionBeginInfo xrSessionBegin = XrSessionBeginInfo.malloc()
		.type$Default()
		.next(MemoryUtil.NULL)
		.primaryViewConfigurationType(XR10.XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO);

	public void begin() {
		XR10.xrBeginSession(xrSession, xrSessionBegin);
		running = true;
	}

	public void requestExit() {
		XR10.xrRequestExitSession(xrSession);
	}

	public void end() {
		XR10.xrEndSession(xrSession);
		running = false;
		ended = true;
	}

	private XrFrameState xrFrameState = XrFrameState.calloc().type$Default();
	private XrFrameWaitInfo xrFrameWaitInfo = XrFrameWaitInfo.calloc().type$Default();
	private XrFrameBeginInfo xrFrameBeginInfo = XrFrameBeginInfo.calloc().type$Default();
	private XrFrameEndInfo xrFrameEndInfo = XrFrameEndInfo.calloc().type$Default().next(MemoryUtil.NULL);

	private XrCompositionLayerProjection layerProjection = XrCompositionLayerProjection.calloc().type$Default();
	private PointerBuffer layers = MemoryUtil.memCallocPointer(1).put(0, layerProjection);

	public void waitOnFrame() {
		XR10.xrWaitFrame(xrSession, xrFrameWaitInfo, xrFrameState);
	}

	public void beginFrame() {
		XR10.xrBeginFrame(xrSession, xrFrameBeginInfo);
	}

	public void endFrame() {
		layerProjection.space(xrSpace).views(xrCLPViews);
		XR10.xrEndFrame(xrSession,
			xrFrameEndInfo.displayTime(xrFrameState.predictedDisplayTime())
				.environmentBlendMode(XR10.XR_ENVIRONMENT_BLEND_MODE_OPAQUE)
				.layers(layers)
				.layerCount(1));
	}

	public void endFrameWithoutRendering() {
		XR10.xrEndFrame(xrSession,
			xrFrameEndInfo.displayTime(xrFrameState.predictedDisplayTime())
				.environmentBlendMode(XR10.XR_ENVIRONMENT_BLEND_MODE_OPAQUE)
				.layers(null)
				.layerCount(0));
	}

	private final OpenXrFrameState frameState = new OpenXrFrameState();

	public OpenXrFrameState getFrameState() {
		return frameState;
	}

	public class OpenXrFrameState {
		private OpenXrFrameState() {
		}

		public long predictedDisplayTime() {
			return xrFrameState.predictedDisplayTime();
		}

		public long predictedDisplayPeriod() {
			return xrFrameState.predictedDisplayPeriod();
		}

		public boolean shouldRender() {
			return xrFrameState.shouldRender();
		}
	}

	private XrView.Buffer xrViews;
	private XrCompositionLayerProjectionView.Buffer xrCLPViews;

	public OpenXRView[] initViews(Seq<ViewConfig> viewConfigs, long imageFormat) {
		xrViews = OpenXRCalls.fill(XrView.calloc(viewConfigs.size()), XrView.TYPE, XR10.XR_TYPE_VIEW);
		xrCLPViews = OpenXRCalls.fill(XrCompositionLayerProjectionView.calloc(viewConfigs.size()),
			XrCompositionLayerProjectionView.TYPE, XR10.XR_TYPE_COMPOSITION_LAYER_PROJECTION_VIEW);
		OpenXRView[] views = new OpenXRView[viewConfigs.size()];
		for (int i = 0; i < views.length; i++) {
			views[i] = createImageSwapchain(viewConfigs.get(i), imageFormat, xrViews.get(i), xrCLPViews.get(i));
		}
		return views;
	}

	private OpenXRView createImageSwapchain(ViewConfig viewConfig, long format, XrView xrView,
		XrCompositionLayerProjectionView xrCLPView) {

		try (MemoryStack stack = MemoryStack.stackPush()) {
			XrSwapchainCreateInfo swapchainCI = XrSwapchainCreateInfo.malloc(stack)
				.type$Default()
				.next(MemoryUtil.NULL)
				.createFlags(0)
				.usageFlags(XR10.XR_SWAPCHAIN_USAGE_SAMPLED_BIT | XR10.XR_SWAPCHAIN_USAGE_COLOR_ATTACHMENT_BIT)
				.format(format)
				.sampleCount(viewConfig.recommendedSwapchainSampleCount())
				.width(viewConfig.recommendedImageRectWidth())
				.height(viewConfig.recommendedImageRectHeight())
				.faceCount(1)
				.arraySize(1)
				.mipCount(1);

			final ToIntFunction<PointerBuffer> fSwapchain = bSwapchain -> XR10.xrCreateSwapchain(xrSession, swapchainCI,
				bSwapchain);
			return OpenXRCalls.toTry(MemoryUtil.memAllocPointer(1), fSwapchain, ERR_CREATE_SWAPCHAIN)
				.mapTry(bSwapchain -> {
					return new OpenXRView(xrView, xrCLPView, new XrSwapchain(bSwapchain.get(0), xrSession),
						swapchainCI.width(), swapchainCI.height());
				})
				.get();
		}
	}

	private XrViewState xrViewState = XrViewState.calloc().type$Default();
	private XrViewLocateInfo xrViewLocateInfo;

	private void initViewLocateInfo() {
		xrViewLocateInfo = XrViewLocateInfo.malloc()
			.type$Default()
			.next(MemoryUtil.NULL)
			.viewConfigurationType(XR10.XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO)
			.space(xrSpace);
	}

	private IntBuffer xrViewCount = MemoryUtil.memAllocInt(1);

	public int getViewCount() {
		return xrViewCount.get(0);
	}

	public void locateViews(long displayTime) {
		XR10.xrLocateViews(xrSession, xrViewLocateInfo.displayTime(displayTime), xrViewState.viewStateFlags(0),
			xrViewCount, xrViews);
	}

	private OpenXrViewState viewState = new OpenXrViewState();

	public OpenXrViewState getViewState() {
		return viewState;
	}

	public final class OpenXrViewState {
		private OpenXrViewState() {
		}

		public boolean isPositionValid() {
			return (xrViewState.viewStateFlags() & XR10.XR_VIEW_STATE_POSITION_VALID_BIT) > 0;
		}

		public boolean isOrientationValid() {
			return (xrViewState.viewStateFlags() & XR10.XR_VIEW_STATE_ORIENTATION_VALID_BIT) > 0;
		}

		public boolean isPositionTracked() {
			return (xrViewState.viewStateFlags() & XR10.XR_VIEW_STATE_POSITION_TRACKED_BIT) > 0;
		}

		public boolean isOrientationTracked() {
			return (xrViewState.viewStateFlags() & XR10.XR_VIEW_STATE_ORIENTATION_TRACKED_BIT) > 0;
		}
	}

	public void attach(OpenXRActionSet actionSet) {
		attach(actionSet, new OpenXRActionSet[0]);
	}

	public void attach(OpenXRActionSet actionSet, OpenXRActionSet... more) {
		try (MemoryStack stack = MemoryStack.stackPush()) {
			final PointerBuffer actionSets = stack.callocPointer(more.length + 1);
			actionSets.put(actionSet.xrActionSet);
			for (int i = 0; i < more.length; i++) {
				actionSets.put(more[i].xrActionSet);
			}
			final XrSessionActionSetsAttachInfo actionSetsAI = XrSessionActionSetsAttachInfo.calloc(stack)
				.type$Default()
				.actionSets(actionSets.rewind());
			int result = XR10.xrAttachSessionActionSets(xrSession, actionSetsAI);
			if (!XR10.XR_SUCCEEDED(result)) {
				throw new OpenXRError(result, "Failed to attach action sets");
			}
		}
	}

	public void poll(OpenXRActionSet actionSet) {
		poll(actionSet, new OpenXRActionSet[0]);
	}

	public void poll(OpenXRActionSet actionSet, OpenXRActionSet... more) {
		try (MemoryStack stack = MemoryStack.stackPush()) {
			final XrActiveActionSet.Buffer actionSets = XrActiveActionSet.calloc(more.length + 1, stack);
			actionSets.get(0).actionSet(actionSet.xrActionSet).subactionPath(XR10.XR_NULL_PATH);
			for (int i = 0; i < more.length; i++) {
				actionSets.get(i + 1).actionSet(more[i].xrActionSet).subactionPath(XR10.XR_NULL_PATH);
			}
			final XrActionsSyncInfo actionsSI = XrActionsSyncInfo.calloc(stack)
				.type$Default()
				.countActiveActionSets(more.length + 1)
				.activeActionSets(actionSets.rewind());
			int result = XR10.xrSyncActions(xrSession, actionsSI);
			if (!XR10.XR_SUCCEEDED(result)) {
				throw new OpenXRError(result, "Failed to sync actions");
			} else if (result == XR10.XR_SESSION_NOT_FOCUSED) {
				return;
			}
		}
		actionSet.pollActions(this);
		for (int i = 0; i < more.length; i++) {
			more[i].pollActions(this);
		}
	}

	public void pollPoses(OpenXRActionSet actionSet, long time) {
		actionSet.pollPoses(this, time);
	}

	void pollBooleanAction(XrActionStateGetInfo actionStateGI, XrActionStateBoolean booleanState) {
		int result = XR10.xrGetActionStateBoolean(xrSession, actionStateGI, booleanState);
		if (!XR10.XR_SUCCEEDED(result)) {
			throw new OpenXRError(result, "Failed to read boolean action");
		}
	}

	void pollFloatAction(XrActionStateGetInfo actionStateGI, XrActionStateFloat floatState) {
		int result = XR10.xrGetActionStateFloat(xrSession, actionStateGI, floatState);
		if (!XR10.XR_SUCCEEDED(result)) {
			throw new OpenXRError(result, "Failed to read float action");
		}
	}

	void pollVector2fAction(XrActionStateGetInfo actionStateGI, XrActionStateVector2f vector2fState) {
		int result = XR10.xrGetActionStateVector2f(xrSession, actionStateGI, vector2fState);
		if (!XR10.XR_SUCCEEDED(result)) {
			throw new OpenXRError(result, "Failed to read Vector2f action");
		}
	}

	void pollPoseAction(XrActionStateGetInfo actionStateGI, XrActionStatePose poseState) {
		int result = XR10.xrGetActionStatePose(xrSession, actionStateGI, poseState);
		if (!XR10.XR_SUCCEEDED(result)) {
			throw new OpenXRError(result, "Failed to read Pose action");
		}
	}

	void pollPoseAction(XrSpace xrActionSpace, long time, XrSpaceLocation xrSpaceLocation) {
		int result = XR10.xrLocateSpace(xrActionSpace, xrSpace, time, xrSpaceLocation);
		if (!XR10.XR_SUCCEEDED(result) && result != XR10.XR_ERROR_TIME_INVALID) {
			throw new OpenXRError(result, "Failed to read Pose location");
		}
	}
}
