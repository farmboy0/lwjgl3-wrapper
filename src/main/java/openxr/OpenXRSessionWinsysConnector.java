package openxr;

import org.lwjgl.openxr.XrSessionCreateInfo;
import org.lwjgl.system.MemoryStack;

public interface OpenXRSessionWinsysConnector {
	OpenXRSessionWinsysConnector useHeadset(long headsetId);

	OpenXRSessionWinsysConnector onStack(MemoryStack stack);

	XrSessionCreateInfo build();
}
