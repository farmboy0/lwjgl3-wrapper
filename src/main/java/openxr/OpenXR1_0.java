package openxr;

import static openxr.OpenXRCalls.fill;
import static openxr.OpenXRCalls.toPointerBuffer;
import static openxr.OpenXRCalls.toTryCustom;
import static org.lwjgl.system.MemoryUtil.memFree;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.function.IntFunction;
import java.util.function.ToIntBiFunction;
import java.util.function.ToIntFunction;

import io.vavr.collection.List;
import io.vavr.collection.Seq;

import org.lwjgl.PointerBuffer;
import org.lwjgl.openxr.XR10;
import org.lwjgl.openxr.XrApiLayerProperties;
import org.lwjgl.openxr.XrApplicationInfo;
import org.lwjgl.openxr.XrExtensionProperties;
import org.lwjgl.openxr.XrInstance;
import org.lwjgl.openxr.XrInstanceCreateInfo;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.MemoryUtil;

public class OpenXR1_0 {
	private static final String ERR_CREATE_INSTANCE = "Failed to create OpenXR instance";
	private static final String ERR_GET_LAYER_NAMES = "Failed to enumerate layers";
	private static final String ERR_GET_EXT_NAMES = "Failed to enumerate instance extensions";

	private OpenXR1_0() {
	}

	public static OpenXRInstance createInstance(String appName, Seq<String> extensions, Seq<String> layers) {
		return createInstance(XR10.XR_API_VERSION_1_0, appName, extensions, layers);
	}

	static OpenXRInstance createInstance(long apiVersion, String appName, Seq<String> extensions, Seq<String> layers) {
		try (MemoryStack stack = MemoryStack.stackPush()) {
			final XrApplicationInfo appInfo = XrApplicationInfo.calloc(stack)
				.applicationName(stack.UTF8(appName))
				.apiVersion(apiVersion);

			final PointerBuffer extensionNames = toPointerBuffer(extensions, stack);
			final PointerBuffer layerNames = toPointerBuffer(layers, stack);
			final XrInstanceCreateInfo instanceCI = XrInstanceCreateInfo.calloc(stack)
				.type$Default()
				.next(MemoryUtil.NULL)
				.createFlags(0)
				.applicationInfo(appInfo)
				.enabledApiLayerNames(layerNames)
				.enabledExtensionNames(extensionNames);

			final ToIntFunction<PointerBuffer> fInstance = bInstance -> XR10.xrCreateInstance(instanceCI, bInstance);
			return OpenXRCalls.toTryStack(stack.mallocPointer(1), fInstance, ERR_CREATE_INSTANCE).mapTry(bInstance -> {
				return new OpenXRInstance(new XrInstance(bInstance.get(0), instanceCI));
			}).get();
		}
	}

	public static final Seq<String> enumInstanceExtensions() {
		final ToIntBiFunction<IntBuffer, XrExtensionProperties.Buffer> fInstExtensions = //
			(bCount, bInstExtensions) -> XR10.xrEnumerateInstanceExtensionProperties((ByteBuffer) null, bCount,
				bInstExtensions);
		final IntFunction<XrExtensionProperties.Buffer> bufferCreator = count -> {
			return fill(XrExtensionProperties.calloc(count), XrExtensionProperties.TYPE,
				XR10.XR_TYPE_EXTENSION_PROPERTIES);
		};

		return toTryCustom(bufferCreator, fInstExtensions, ERR_GET_EXT_NAMES).mapTry(tInstExtensions -> {
			final Seq<String> result = List.range(0, tInstExtensions._1)
				.map(tInstExtensions._2::get)
				.map(XrExtensionProperties::extensionNameString);
			memFree(tInstExtensions._2);
			return result;
		}).get();
	}

	public static final Seq<String> enumApiLayers() {
		final ToIntBiFunction<IntBuffer, XrApiLayerProperties.Buffer> fLayers = XR10::xrEnumerateApiLayerProperties;
		final IntFunction<XrApiLayerProperties.Buffer> bufferCreator = count -> {
			return fill(XrApiLayerProperties.calloc(count), XrApiLayerProperties.TYPE,
				XR10.XR_TYPE_API_LAYER_PROPERTIES);
		};

		return toTryCustom(bufferCreator, fLayers, ERR_GET_LAYER_NAMES).mapTry(tLayers -> {
			final Seq<String> result = List.range(0, tLayers._1)
				.map(tLayers._2::get)
				.map(XrApiLayerProperties::layerNameString);
			memFree(tLayers._2);
			return result;
		}).get();
	}
}
