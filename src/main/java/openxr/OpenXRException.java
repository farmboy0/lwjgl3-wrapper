package openxr;

public class OpenXRException extends RuntimeException {

	OpenXRException() {
	}

	OpenXRException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	OpenXRException(String message, Throwable cause) {
		super(message, cause);
	}

	OpenXRException(String message) {
		super(message);
	}

	OpenXRException(Throwable cause) {
		super(cause);
	}
}
