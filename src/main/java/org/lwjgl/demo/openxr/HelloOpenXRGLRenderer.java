/*
 * Copyright LWJGL. All rights reserved.
 * License terms: https://www.lwjgl.org/license
 */
package org.lwjgl.demo.openxr;

import static org.lwjgl.opengl.GL11.GL_BACK;
import static org.lwjgl.opengl.GL11.GL_CCW;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_CULL_FACE;
import static org.lwjgl.opengl.GL11.GL_CW;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_COMPONENT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL11.GL_NEAREST;
import static org.lwjgl.opengl.GL11.GL_STENCIL_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MAG_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MIN_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_S;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_T;
import static org.lwjgl.opengl.GL11.GL_TRIANGLES;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_SHORT;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.opengl.GL11.glClearDepth;
import static org.lwjgl.opengl.GL11.glCullFace;
import static org.lwjgl.opengl.GL11.glDeleteTextures;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glDrawArrays;
import static org.lwjgl.opengl.GL11.glDrawElements;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glFinish;
import static org.lwjgl.opengl.GL11.glFlush;
import static org.lwjgl.opengl.GL11.glFrontFace;
import static org.lwjgl.opengl.GL11.glGenTextures;
import static org.lwjgl.opengl.GL11.glTexImage2D;
import static org.lwjgl.opengl.GL11.glTexParameteri;
import static org.lwjgl.opengl.GL11.glViewport;
import static org.lwjgl.opengl.GL12.GL_CLAMP_TO_EDGE;
import static org.lwjgl.opengl.GL14.GL_DEPTH_COMPONENT32;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_ELEMENT_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_STATIC_DRAW;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glDeleteBuffers;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import static org.lwjgl.opengl.GL20.glDeleteProgram;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glGetUniformLocation;
import static org.lwjgl.opengl.GL20.glUniformMatrix4fv;
import static org.lwjgl.opengl.GL20.glUseProgram;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.GL_COLOR_ATTACHMENT0;
import static org.lwjgl.opengl.GL30.GL_DEPTH_ATTACHMENT;
import static org.lwjgl.opengl.GL30.GL_FRAMEBUFFER;
import static org.lwjgl.opengl.GL30.glBindFramebuffer;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glDeleteFramebuffers;
import static org.lwjgl.opengl.GL30.glDeleteVertexArrays;
import static org.lwjgl.opengl.GL30.glFramebufferTexture2D;
import static org.lwjgl.opengl.GL30.glGenFramebuffers;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.HashMap;
import java.util.Map;

import org.joml.Matrix4f;
import org.joml.Quaternionfc;
import org.joml.Vector3fc;
import org.lwjgl.BufferUtils;

import glfw.GLFWRuntime;
import openxr.OpenXRView;
import openxr.OpenXRView.ProjectionView;

public class HelloOpenXRGLRenderer {

	//GL globals
	Map<Integer, Integer> depthTextures; //Swapchain images only provide a color texture so we have to create depth textures seperatley

	int swapchainFramebuffer;
	int cubeVertexBuffer;
	int cubeIndexBuffer;
	int quadVertexBuffer;
	int cubeVAO;
	int quadVAO;
	int screenShader;
	int textureShader;
	int colorShader;

	private OpenXRView[] views;

	public HelloOpenXRGLRenderer(OpenXRView[] views) {
		this.views = views;
		init();
	}

	public void finish() {
		// Wait until idle
		glFinish();

		//Destroy OpenGL
		for (int texture : depthTextures.values()) {
			glDeleteTextures(texture);
		}
		glDeleteFramebuffers(swapchainFramebuffer);
		glDeleteBuffers(cubeVertexBuffer);
		glDeleteBuffers(cubeIndexBuffer);
		glDeleteBuffers(quadVertexBuffer);
		glDeleteVertexArrays(cubeVAO);
		glDeleteVertexArrays(quadVAO);
		glDeleteProgram(screenShader);
		glDeleteProgram(textureShader);
		glDeleteProgram(colorShader);
	}

	private void init() {
		swapchainFramebuffer = glGenFramebuffers();
		depthTextures = new HashMap<>(0);
		for (OpenXRView view : views) {
			for (int i = 0; i < view.getImageCount(); i++) {
				int texture = glGenTextures();
				glBindTexture(GL_TEXTURE_2D, texture);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
				glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, view.getWidth(), view.getHeight(), 0,
					GL_DEPTH_COMPONENT, GL_FLOAT, (ByteBuffer) null);
				depthTextures.put(view.getTexture(i), texture);
			}
		}
		glBindTexture(GL_TEXTURE_2D, 0);

		textureShader = ShadersGL.createShaderProgram(ShadersGL.texVertShader, ShadersGL.texFragShader);
		colorShader = ShadersGL.createShaderProgram(ShadersGL.colVertShader, ShadersGL.colFragShader);
		screenShader = ShadersGL.createShaderProgram(ShadersGL.screenVertShader, ShadersGL.texFragShader);

		{
			cubeVertexBuffer = glGenBuffers();
			glBindBuffer(GL_ARRAY_BUFFER, cubeVertexBuffer);
			glBufferData(GL_ARRAY_BUFFER, Geometry.cubeVertices, GL_STATIC_DRAW);

			cubeIndexBuffer = glGenBuffers();
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, cubeIndexBuffer);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, Geometry.cubeIndices, GL_STATIC_DRAW);

			cubeVAO = glGenVertexArrays();
			glBindVertexArray(cubeVAO);
			glEnableVertexAttribArray(0);
			glEnableVertexAttribArray(1);
			glVertexAttribPointer(0, 3, GL_FLOAT, false, 4 * 3 * 2, 0);
			glVertexAttribPointer(1, 3, GL_FLOAT, false, 24, 12);
		}
		{
			quadVAO = glGenVertexArrays();
			quadVertexBuffer = glGenBuffers();
			glBindVertexArray(quadVAO);
			glBindBuffer(GL_ARRAY_BUFFER, quadVertexBuffer);
			glBufferData(GL_ARRAY_BUFFER, Geometry.quadVertices, GL_STATIC_DRAW);
			glEnableVertexAttribArray(0);
			glVertexAttribPointer(0, 2, GL_FLOAT, false, 4 * 4, 0);
			glEnableVertexAttribArray(1);
			glVertexAttribPointer(1, 2, GL_FLOAT, false, 4 * 4, 2 * 4);
		}

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
	}

	private static Matrix4f modelviewMatrix = new Matrix4f();
	private static Matrix4f projectionMatrix = new Matrix4f();
	private static Matrix4f viewMatrix = new Matrix4f();

	private static FloatBuffer mvpMatrix = BufferUtils.createFloatBuffer(16);

	public void render(OpenXRView view, int viewIndex, int ww, int wh) {
		glBindFramebuffer(GL_FRAMEBUFFER, swapchainFramebuffer);

		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, view.getCurrentTexture(), 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D,
			depthTextures.get(view.getCurrentTexture()), 0);

		ProjectionView projView = view.getProjectionView();
		glViewport(projView.getSubImageOffsetX(), projView.getSubImageOffsetY(), projView.getSubImageWidth(),
			projView.getSubImageHeight());

		float[] DarkSlateGray = { 0.184313729f, 0.309803933f, 0.309803933f };
		glClearColor(DarkSlateGray[0], DarkSlateGray[1], DarkSlateGray[2], 1.0f);
		glClearDepth(1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

		glFrontFace(GL_CW);
		glCullFace(GL_BACK);
		glEnable(GL_DEPTH_TEST);

		float distToLeftPlane = org.joml.Math.tan(projView.getFoVAngleLeft()) * 0.1f;
		float distToRightPlane = org.joml.Math.tan(projView.getFoVAngleRight()) * 0.1f;
		float distToBottomPlane = org.joml.Math.tan(projView.getFoVAngleDown()) * 0.1f;
		float distToTopPlane = org.joml.Math.tan(projView.getFoVAngleUp()) * 0.1f;
		projectionMatrix.setFrustum(distToLeftPlane, distToRightPlane, distToBottomPlane, distToTopPlane, 0.1f, 100f,
			false);

		Vector3fc pos = projView.getPosition();
		Quaternionfc orientation = projView.getOrientation();
		viewMatrix.translationRotateScaleInvert(pos.x(), pos.y(), pos.z(), orientation.x(), orientation.y(),
			orientation.z(), orientation.w(), 1, 1, 1);

		glDisable(GL_CULL_FACE); // Disable back-face culling so we can see the inside of the world-space cube and backside of the plane

		{ // Rotating plane
			modelviewMatrix.translation(0, 0, -3).rotate((float) -GLFWRuntime.time(), 1, 0, 0);
			glUseProgram(colorShader);
			glUniformMatrix4fv(glGetUniformLocation(colorShader, "projection"), false, projectionMatrix.get(mvpMatrix));
			glUniformMatrix4fv(glGetUniformLocation(colorShader, "view"), false, viewMatrix.get(mvpMatrix));
			glUniformMatrix4fv(glGetUniformLocation(colorShader, "model"), false, modelviewMatrix.get(mvpMatrix));
			glBindVertexArray(quadVAO);
			glDrawArrays(GL_TRIANGLES, 0, 6);
		}

		{ // World-space cube
			modelviewMatrix.identity().scale(10);
			glUniformMatrix4fv(glGetUniformLocation(colorShader, "model"), false, modelviewMatrix.get(mvpMatrix));
			glBindVertexArray(cubeVAO);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, cubeIndexBuffer);
			glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_SHORT, 0);
		}

		glEnable(GL_CULL_FACE);

		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		int wh2 = (int) (((float) view.getHeight() / view.getWidth()) * ww);
		if (wh2 > wh) {
			int ww2 = (int) (((float) view.getWidth() / view.getHeight()) * wh);
			glViewport(ww2 * viewIndex, 0, ww2, wh);
		} else {
			glViewport(ww * viewIndex, 0, ww, wh2);
		}
		glFrontFace(GL_CCW);
		glUseProgram(screenShader);
		glBindVertexArray(quadVAO);
		glDisable(GL_DEPTH_TEST);
		glBindTexture(GL_TEXTURE_2D, view.getCurrentTexture());
		glDrawArrays(GL_TRIANGLES, 0, 6);
		if (viewIndex == views.length - 1) {
			glFlush();
		}
	}

	private static class Geometry {

		static float[] cubeVertices = { -0.5f, 0.5f, -0.5f, 0.25f, 0f, 0f, -0.5f, -0.5f, 0.5f, 0.25f, 0f, 0f, -0.5f,
			-0.5f, -0.5f, 0.25f, 0f, 0f, -0.5f, 0.5f, -0.5f, 0.25f, 0f, 0f, -0.5f, 0.5f, 0.5f, 0.25f, 0f, 0f, -0.5f,
			-0.5f, 0.5f, 0.25f, 0f, 0f, 0.5f, 0.5f, -0.5f, 1f, 0f, 0f, 0.5f, -0.5f, -0.5f, 1f, 0f, 0f, 0.5f, -0.5f,
			0.5f, 1f, 0f, 0f, 0.5f, 0.5f, -0.5f, 1f, 0f, 0f, 0.5f, -0.5f, 0.5f, 1f, 0f, 0f, 0.5f, 0.5f, 0.5f, 1f, 0f,
			0f, -0.5f, -0.5f, -0.5f, 0f, 0.25f, 0f, -0.5f, -0.5f, 0.5f, 0f, 0.25f, 0f, 0.5f, -0.5f, 0.5f, 0f, 0.25f, 0f,
			-0.5f, -0.5f, -0.5f, 0f, 0.25f, 0f, 0.5f, -0.5f, 0.5f, 0f, 0.25f, 0f, 0.5f, -0.5f, -0.5f, 0f, 0.25f, 0f,
			-0.5f, 0.5f, -0.5f, 0f, 1f, 0f, 0.5f, 0.5f, -0.5f, 0f, 1f, 0f, 0.5f, 0.5f, 0.5f, 0f, 1f, 0f, -0.5f, 0.5f,
			-0.5f, 0f, 1f, 0f, 0.5f, 0.5f, 0.5f, 0f, 1f, 0f, -0.5f, 0.5f, 0.5f, 0f, 1f, 0f, -0.5f, -0.5f, -0.5f, 0f, 0f,
			0.25f, 0.5f, -0.5f, -0.5f, 0f, 0f, 0.25f, 0.5f, 0.5f, -0.5f, 0f, 0f, 0.25f, -0.5f, -0.5f, -0.5f, 0f, 0f,
			0.25f, 0.5f, 0.5f, -0.5f, 0f, 0f, 0.25f, -0.5f, 0.5f, -0.5f, 0f, 0f, 0.25f, -0.5f, -0.5f, 0.5f, 0f, 0f, 1f,
			-0.5f, 0.5f, 0.5f, 0f, 0f, 1f, 0.5f, 0.5f, 0.5f, 0f, 0f, 1f, -0.5f, -0.5f, 0.5f, 0f, 0f, 1f, 0.5f, 0.5f,
			0.5f, 0f, 0f, 1f, 0.5f, -0.5f, 0.5f, 0f, 0f, 1f };

		// Winding order is clockwise. Each side uses a different color.
		static short[] cubeIndices = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22,
			23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, };

		static float[] quadVertices = { // vertex attributes for a quad that fills the entire screen in Normalized Device Coordinates.
			// positions   // texCoords
			-1.0f, 1.0f, 0.0f, 1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, -1.0f, 1.0f, 0.0f,

			-1.0f, 1.0f, 0.0f, 1.0f, 1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f };
	}
}