/*
 * Copyright LWJGL. All rights reserved.
 * License terms: https://www.lwjgl.org/license
 */
package org.lwjgl.demo.openxr;

import static org.lwjgl.opengl.GL11.GL_RGB10_A2;
import static org.lwjgl.opengl.GL11.GL_RGBA8;
import static org.lwjgl.opengl.GL11.glGetInteger;
import static org.lwjgl.opengl.GL30.GL_MAJOR_VERSION;
import static org.lwjgl.opengl.GL30.GL_MINOR_VERSION;
import static org.lwjgl.opengl.GL30.GL_RGBA16F;

import io.vavr.API;
import io.vavr.collection.Seq;

import org.joml.Quaternionfc;
import org.joml.Vector3fc;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL21;
import org.lwjgl.opengl.GL31;
import org.lwjgl.opengl.GLUtil;

import glfw.GLFWException;
import glfw.GLFWRuntime;
import glfw.GLFWWindow;
import glfw.GLFWWindowHintsBuilder;
import openxr.OpenXR1_0;
import openxr.OpenXRAPILayer;
import openxr.OpenXRDebugListener;
import openxr.OpenXREventListener;
import openxr.OpenXRException;
import openxr.OpenXRExtension;
import openxr.OpenXRInstance;
import openxr.OpenXRInstance.OpenXRGLSupport;
import openxr.OpenXRInstance.ViewConfig;
import openxr.OpenXRSession;
import openxr.OpenXRSessionState;
import openxr.OpenXRView;
import openxr_glfw.OpenXRSessionGLFWConnector;

public class HelloOpenXRGL {

	GLFWWindow window;

	//XR globals
	//Init
	OpenXRInstance xrInstance;
	OpenXRSession xrSession;
	boolean useEglGraphicsBinding;
	long glColorFormat;
	OpenXRView[] views; //One swapchain per view
	Seq<ViewConfig> viewConfigs;

	//Runtime
	OpenXRSessionState sessionState;

	HelloOpenXRGLRenderer renderer;

	public static void main(String[] args) throws Exception {
		HelloOpenXRGL helloOpenXR = new HelloOpenXRGL();

		helloOpenXR.createOpenXRInstance();
		helloOpenXR.registerEventHandler();
		helloOpenXR.initializeAndBindOpenGL();
		helloOpenXR.createXRReferenceSpace();
		helloOpenXR.createXRSwapchains();

		helloOpenXR.renderer = new HelloOpenXRGLRenderer(helloOpenXR.views);

		while (!helloOpenXR.xrSession.isEnded()) {
			if (helloOpenXR.xrSession.isRunning() && helloOpenXR.window.isClosing()) {
				helloOpenXR.xrSession.requestExit();
			}
			helloOpenXR.pollEvents();
			if (helloOpenXR.xrSession.isRunning()) {
				helloOpenXR.renderFrameOpenXR();
			} else {
				// Throttle loop since xrWaitFrame won't be called.
				Thread.sleep(250);
			}
		}

		//Destroy OpenGL
		helloOpenXR.renderer.finish();

		// Destroy OpenXR
		for (OpenXRView view : helloOpenXR.views) {
			view._OpenXRView_();
		}

		helloOpenXR.xrSession._OpenXRSession_();
		helloOpenXR.xrInstance._OpenXRInstance_();

		GLFWRuntime.terminate();
	}

	public void createOpenXRInstance() throws OpenXRException {
		Seq<String> extList = OpenXRExtension.EXTENSIONS;

		System.out.printf("OpenXR loaded with %d extensions:%n", extList.size());
		System.out.println("~~~~~~~~~~~~~~~~~~");
		extList.forEach(System.out::println);

		useEglGraphicsBinding = OpenXRExtension.XR_MNDX_EGL_ENABLE.isAvailable();
		if (useEglGraphicsBinding) {
			System.out.println("Going to use cross-platform experimental EGL for session creation");
		} else {
			System.out.println("Going to use platform-specific session creation");
		}
		System.out.println("~~~~~~~~~~~~~~~~~~");

		Seq<String> extensions = OpenXRExtension.builder()
			.require(OpenXRExtension.XR_KHR_OPENGL_ENABLE)
			.useIfAvailable(OpenXRExtension.XR_EXT_DEBUG_UTILS)
			.useIfAvailable(OpenXRExtension.XR_MNDX_EGL_ENABLE)
			.build();

		boolean useValidationLayer = false;

		Seq<String> layerList = OpenXRAPILayer.API_LAYERS;
		int numLayers = layerList.size();

		System.out.println(numLayers + " XR layers are available:");
		for (int index = 0; index < numLayers; index++) {
			String layerName = layerList.get(index);
			System.out.println(layerName);

			// At the time of wring this, the OpenXR validation layers don't like EGL
			if (!useEglGraphicsBinding && layerName.equals("XR_APILAYER_LUNARG_core_validation")) {
				useValidationLayer = true;
			}
		}
		System.out.println("-----------");

		Seq<String> wantedLayers = API.Seq();
		if (useValidationLayer) {
			wantedLayers = wantedLayers.append("XR_APILAYER_LUNARG_core_validation");
			System.out.println("Enabling XR core validation");
		} else {
			System.out.println("Running without validation layers");
		}

		System.out.println("Creating OpenXR instance...");
		xrInstance = OpenXR1_0.createInstance("HelloOpenXR", extensions, wantedLayers);
		System.out.println("Created OpenXR instance");

		OpenXRDebugListener cb = (severity, channels, messageId, functionName, message, labels) -> {
			System.out.println("XR Debug Utils: " + message);
			return 0;
		};

		System.out.println("Enabling OpenXR debug utils");
		xrInstance.enableDebugging(cb);
	}

	public void initializeAndBindOpenGL() throws GLFWException {
		//Initialize OpenXR's OpenGL compatability
		OpenXRGLSupport glSupport = xrInstance.determineGLSupport();

		System.out.println("The OpenXR runtime supports OpenGL " + glSupport.majorMin() + "." + glSupport.minorMin()
			+ " to OpenGL " + glSupport.majorMax() + "." + glSupport.minorMax());

		// This example needs at least OpenGL 4.0
		if (glSupport.majorMax() < 4) {
			throw new UnsupportedOperationException("This example requires at least OpenGL 4.0");
		}
		int majorVersionToRequest = 4;
		int minorVersionToRequest = 0;

//		// But when the OpenXR runtime requires a later version, we should respect that.
//		// As a matter of fact, the runtime on my current laptop does, so this code is actually needed.
		if (glSupport.majorMin() == 4) {
			minorVersionToRequest = 5;
		}

		GLFWWindowHintsBuilder.INSTANCE.openGLMajorVersion(majorVersionToRequest)
			.openGLMinorVersion(minorVersionToRequest)
			.useCoreProfile()
			.doubleBuffered(false)
			.useEGL(useEglGraphicsBinding)
			.build();

		window = new GLFWWindow(640, 480, "Hello World");
		window.makeCurrent();
		GL.createCapabilities();
		GLUtil.setupDebugMessageCallback();

		// Check if OpenGL version is supported by OpenXR runtime
		int actualMajorVersion = glGetInteger(GL_MAJOR_VERSION);
		int actualMinorVersion = glGetInteger(GL_MINOR_VERSION);

		if (glSupport.majorMin() > actualMajorVersion
			|| (glSupport.majorMin() == actualMajorVersion && glSupport.minorMin() > actualMinorVersion)) {
			throw new IllegalStateException(
				"The OpenXR runtime supports only OpenGL " + glSupport.majorMin() + "." + glSupport.minorMin()
					+ " and later, but we got OpenGL " + actualMajorVersion + "." + actualMinorVersion);
		}

		if (actualMajorVersion > glSupport.majorMax()
			|| (actualMajorVersion == glSupport.majorMax() && actualMinorVersion > glSupport.minorMax())) {
			throw new IllegalStateException(
				"The OpenXR runtime supports only OpenGL " + glSupport.majorMax() + "." + glSupport.majorMin()
					+ " and earlier, but we got OpenGL " + actualMajorVersion + "." + actualMinorVersion);
		}

		xrSession = xrInstance.createSessionFor(new OpenXRSessionGLFWConnector(window, useEglGraphicsBinding));
	}

	public void createXRReferenceSpace() {
		xrSession.createReferenceSpace();
	}

	public void createXRSwapchains() {
		System.out.printf("Headset name: %s, vendor: %d%n", xrInstance.getSystemName(), xrInstance.getVendorId());
		System.out.printf("Headset orientationTracking: %b, positionTracking: %b%n",
			xrInstance.hasOrientationTracking(), xrInstance.hasPositionTracking());
		System.out.printf("Headset MaxWidth: %d, MaxHeight: %d, MaxLayerCount: %d%n",
			xrInstance.getMaxSwapchainImageWidth(), xrInstance.getMaxSwapchainImageHeight(),
			xrInstance.getMaxLayerCount());

		Seq<Long> swapchainFormats = xrSession.enumSwapchainFormats();

		long[] desiredSwapchainFormats = { GL_RGB10_A2, GL_RGBA16F,
			// The two below should only be used as a fallback, as they are linear color formats without enough bits for color
			// depth, thus leading to banding.
			GL_RGBA8, GL31.GL_RGBA8_SNORM, GL21.GL_SRGB8_ALPHA8 };

		long glColorFormat = 0;

		out: for (long glFormatIter : desiredSwapchainFormats) {
			for (int i = 0; i < swapchainFormats.size(); i++) {
				if (glFormatIter == swapchainFormats.get(i)) {
					glColorFormat = glFormatIter;
					break out;
				}
			}
		}

		if (glColorFormat == 0) {
			throw new IllegalStateException("No compatable swapchain / framebuffer format availible");
		}

		viewConfigs = xrInstance.enumViewConfigurationViews();
		int viewCountNumber = viewConfigs.size();

		if (viewCountNumber <= 0) {
			System.err.println("No view configs found");
			return;
		}

		views = xrSession.initViews(viewConfigs, glColorFormat);
	}

	private void pollEvents() {
		GLFWRuntime.pollEvents();
		xrInstance.pollEvents();
	}

	private void registerEventHandler() {
		xrInstance.register(new OpenXREventListener() {

			@Override
			public void sessionStateChange(long session, OpenXRSessionState state, long time) {
				OpenXRHandleSessionStateChangedEvent(session, state, time);
			}

			@Override
			public void refSpaceChange(long session, int referenceSpaceType, long changeTime, boolean poseValid,
				Vector3fc positionInPreviousSpace, Quaternionfc orientationInPreviousSpace) {
				System.out.printf("Ignoring event type: Reference Space Change%n");
			}

			@Override
			public void interactionProfileChange(long session) {
				System.out.printf("Ignoring event type: Interaction Profile Change%n");
			}

			@Override
			public void instanceLoss(long lossTime) {
				System.err.printf("XrEventDataInstanceLossPending by %d%n", lossTime);
			}

			@Override
			public void eventsLost(int lostEventCount) {
				System.out.printf("%d events lost%n", lostEventCount);
			}
		});
	}

	void OpenXRHandleSessionStateChangedEvent(long session, OpenXRSessionState state, long time) {
		OpenXRSessionState oldState = sessionState;
		sessionState = state;

		System.out.printf("XrEventDataSessionStateChanged: state %s->%s session=%d time=%d\n", oldState, sessionState,
			session, time);

		if (OpenXRSessionState.READY.equals(state)) {
			assert (xrSession != null);
			xrSession.begin();
		} else if (OpenXRSessionState.STOPPING.equals(state)) {
			assert (xrSession != null);
			xrSession.end();
		}
	}

	private void renderFrameOpenXR() {
		xrSession.waitOnFrame();
		xrSession.beginFrame();

		boolean didRender = false;
		if (xrSession.getFrameState().shouldRender()) {
			if (renderLayerOpenXR(xrSession.getFrameState().predictedDisplayTime())) {
				didRender = true;
			} else {
				System.out.println("Didn't render");
			}
		} else {
			System.out.println("Shouldn't render");
		}

		if (didRender)
			xrSession.endFrame();
		else
			xrSession.endFrameWithoutRendering();
	}

	private boolean renderLayerOpenXR(long predictedDisplayTime) {

		xrSession.locateViews(predictedDisplayTime);

		if (!xrSession.getViewState().isPositionValid() || !xrSession.getViewState().isOrientationValid()) {
			return false; // There is no valid tracking poses for the views.
		}

		int viewCountOutput = xrSession.getViewCount();
		assert (viewCountOutput == viewConfigs.size());
		assert (viewCountOutput == views.length);

		// Render view to the appropriate part of the swapchain image.
		for (int viewIndex = 0; viewIndex < viewCountOutput; viewIndex++) {
			// Each view has a separate swapchain which is acquired, rendered to, and released.
			OpenXRView view = views[viewIndex];

			view.aquireAndWaitImage();

			view.updateLayerView();

			renderer.render(view, viewIndex, window.getWidth(), window.getHeight());

			view.releaseImage();
		}

		return true;
	}
}