xmlstarlet sel -t -v \
    "//extensions/extension/require/enum[contains(@name,'EXTENSION_NAME')]/@value" \
    -nl  xr.xml \
| sed 's/"//g' \
| grep -v '_[[:digit:]]\{2,3\}$' \
| sed -E 's|(.+)|\U\1\E\("\1"\), //|g' \
| sort

